const COLORS = {
    white: '#fff',
    dark: '#000',
    red: '#F52A2A',
    light: '#F1F1F1',
    green: '#00B761',
    orange:'#FBA208',
    parat:'#92A21E'
  };
  
  export default COLORS;