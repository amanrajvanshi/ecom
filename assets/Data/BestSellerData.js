const BestSellerData =[
    {
        id: 1,
        name: 'Succulent ',
        price: '39.99',
        like: true,
        tag:'fruits $vegitable',
        weight:'1kg',
        brand:'high',
        country:'India',
        foodType:'fresh',
        Manufacturer:'Private lable',
        img: require('../../assets/orange.jpg'),
        about:
          'Succulent Plantis one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
      },
    
      {
        id: 2,
        name: 'Dragon ',
        price: '29.99',
        like: false,
        tag:'fruits $vegitable',
        weight:'1kg',
        brand:'high',
        country:'India',
        foodType:'fresh',
        Manufacturer:'Private lable',
        img: require('../../assets/apple.jpg'),      
        about:
          'Dragon Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
      },
      {
        id: 3,
        name: 'Ravenea ',
        price: '25.99',
        tag:'fruits $vegitable',
        weight:'1kg',
        brand:'high',
        country:'India',
        foodType:'fresh',
        like: false,
        Manufacturer:'Private lable',
        img: require('../../assets/sort.png'),     
         about:
          'Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
      },
    
      {
        id: 4,
        name: 'Potted ',
        price: '25.99',
        like: true,
        tag:'fruits $vegitable',
        weight:'1kg',
        brand:'high',
        country:'India',
        foodType:'fresh',
        Manufacturer:'Private lable',
        img: require('../../assets/sort.png'),      about:
          'Potted Plant Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
      },
      {
        id: 5,
        name: 'Ravenea ',
        price: '50.99',
        like: true,
        tag:'fruits $vegitable',
        weight:'1kg',
        brand:'high',
        country:'India',
        foodType:'fresh',
        Manufacturer:'Private lable',
        img: require('../../assets/sort.png'),      about:
          'Potted Plant Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
      },
      {
        id: 6,
        name: 'Dragon ',
        price: '50.99',
        like: false,
        tag:'fruits $vegitable',
        weight:'1kg',
        brand:'high',
        country:'India',
        foodType:'fresh',
        Manufacturer:'Private lable',
        img: require('../../assets/sort.png'),      about:
          'Potted Plant Ravenea Plant one of the most popular and beautiful species that will produce clumpms. The storage of water often gives succulent plants a more swollen or fleshy appearance than other plants, a characteristic known as succulence.',
      },

]
export default BestSellerData;