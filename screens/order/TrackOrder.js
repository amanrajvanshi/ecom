import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, TextInput, CheckBox, Image } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';

const labels = ["Order Placed", "Order Confirmed", "Order Shipped", "Out of Delivery", "Order Deliverd"];
const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#fe7013',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 17,
    currentStepLabelColor: '#fe7013',

}




class TrackOrder extends React.Component {




    constructor() {
        super()

        {
            this.state = {
                currentPosition: 3
            }
        }
    }


    render() {
        const data = [
            {
                label: 'Order Placed',
                dateTime: 'Sat, 3rd Nov 11:49pm'
            },
            {
                label: 'Order Confirmed',
                dateTime: 'Sat, 3rd Nov 11:49pm'
            }
            ,
            {
                label: 'Order Shipped',
                dateTime: 'Sat, 3rd Nov 11:49pm'
            },
            {
                label: 'Out Of Delivery',
                dateTime: 'Pending'
            },

            {
                label: 'Order Deliverd',
                dateTime: 'Pending'
            }
        ]




        return (



            <View >
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('checkout')}>


                        <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                    </TouchableOpacity>
                    <View style={styles.searchBar}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 35 }}>Track Order</Text>

                    </View>
                  
                </View>

                <View style={{ borderWidth: 1, borderColor: '#8ebc0e', height: 130, width: 360, alignSelf: 'center', marginTop: 15, borderRadius: 8, backgroundColor: '#8ebc0e', marginBottom: 50 }}>
                    <View style={{ backgroundColor: 'white', width: 90, height: 90, borderRadius: 150 / 2, marginTop: 20, marginLeft: 20 }}>
                        <View style={{ marginLeft: 25, marginTop: 25 }}>
                            <Ionicons name="analytics-outline" size={40} color="#222" backgroundColor="white" />

                        </View>

                    </View>
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'flex-end', marginTop: -85, marginRight: 40 }}>
                        <Text style={{ fontSize: 17, fontWeight: 'bold', color: 'white' }}>Order Id: #OD2204</Text>
                        <Text style={{ fontSize: 15, color: 'white' }}>Placed on jan,26 2021</Text>
                        <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>Items: 15 Total: $100.0</Text>
                    </View>
                </View>
                <View style={{ height: '60%', width: '87%', borderWidth: 1, borderColor: '#DCDCDC', marginTop: -20, alignSelf: 'center', borderRadius: 8, alignItems: 'center' }}>
                    <StepIndicator
                        customStyles={customStyles}
                        currentPosition={this.state.currentPosition}
                        labels={labels}
                        direction="vertical"
                        renderLabel={({ position }) => {
                            return (
                                <View style={{ marginLeft: 70 }}>
                                    <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{data[position].label}</Text>

                                    <Text style={{ fontSize: 12 }}>{data[position].dateTime}</Text>
                                </View>

                            )
                        }}





                    />
                </View>



            </View>


        );


    }
    onPageChange(position) {
        this.setState({ currentPosition: position });
    }
}







export default TrackOrder;

const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 8,
        marginRight: 8

    },





    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },

    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },

})