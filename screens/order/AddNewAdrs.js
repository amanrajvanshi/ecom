import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, Label, TextInput, CheckBox, Image } from 'react-native';

import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';

import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';



const AddNewAdrs = () => {
    const navigation = useNavigation();




    return (



        <View style={{flex:1}} >
            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.goBack()}>


                    <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 45 }}>Edit Address</Text>

                </View>
                {/* <View style={styles.cart}>

                    <Icon1 name="shoppingcart" size={25} color="gray" />

                    <View style={styles.badge}>
                        <Text style={styles.badgeText}>4</Text>
                    </View>
                </View> */}
            </View>

            <ScrollView >
                <View style={{  marginTop: 20, }}>
                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Name</Text>
                        <TextInput

                            placeholder="Enter Your Name"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Email Address</Text>
                        <TextInput
                            textContentType='emailAddress'
                            placeholder="Enter Your Email"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Phone No.</Text>
                        <TextInput
                            textContentType='telephoneNumber'
                            keyboardType='number-pad'
                            maxLength={10}
                            placeholder="Enter Your Phone No"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Address Line1</Text>
                        <TextInput

                            placeholder="Enter Address Line1"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Address Line2</Text>
                        <TextInput

                            placeholder="Enter Address Line2"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Zipcode</Text>
                        <TextInput
                            keyboardType='number-pad'
                            placeholder="Enter Zipcode"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>



                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>State</Text>
                        <TextInput

                            placeholder="Enter Country Name"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Country</Text>
                        <TextInput

                            placeholder="Lucy martin"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15, }}>
                        <Text style={{ marginLeft: 10, marginTop: 15, fontSize: 16, }}>
                            <CircleCheckBox
                                checked={true}
                                innerSize={10}
                                outerSize={18}

                            />
                        </Text>
                        <View>

                            <Text style={{ marginTop: 14, marginLeft: 10, fontWeight: 'bold' }}>

                                Make default shopping address</Text>

                        </View>

                    </View>


                    {/* <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Name</Text>
                        <TextInput

                            placeholder="Lucy martin"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>
                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Name</Text>
                        <TextInput

                            placeholder="Lucy martin"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View> */}

                </View>


            </ScrollView>



            <TouchableOpacity style={styles.buttonStyle}
                onPress={() => navigation.navigate('DeliveryAddress')}

            >
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Save Address</Text>
            </TouchableOpacity>




        </View>














    );

}







export default AddNewAdrs;

const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 8,
        marginRight: 8

    },





    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },

    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        marginTop:'2%',
        marginBottom:'2%',
        backgroundColor: '#8ebc0e',


        borderRadius: 10,
        // position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 365,
        height: 50,
        alignSelf: 'center',
        // marginTop: 550


    },
})