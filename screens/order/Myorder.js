import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,

  TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
// import CardData from '../../CardData'
import MyOrderData from '../../assets/Data/MyOrderData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const width = Dimensions.get('window').width / 1 - 800;



//plant is a props which is given in card component 

function CategoryItem({ navigation }) {


  const Card = ({ plant }) => {

    return (






      <View style={styles.item}>






        <Image source={plant.img} resizeMode="contain" style={styles.img} />
        <View style={{ flexDirection: 'column', marginLeft: 30, flex: 1, justifyContent: 'center',marginTop:"2%" }}>
          <Text >{plant.order}</Text>

          <Text >Rs {plant.price}</Text>
          <Text style={{color:'red'}}>Rs {plant.sumery}</Text>
          <Text >Rs {plant.date}</Text>



          <Text style={{ color: 'gray', fontWeight: 'bold' }}>{plant.quantity}</Text>

        </View>
        <View style={{ backgroundColor: 'orange', width: 40, height: 40, borderRadius: 150 / 2, marginRight: 13 ,alignSelf: 'center',justifyContent:'center',}}>
          <View style={{  alignSelf: 'center'}}>
            <TouchableOpacity onPress={() => navigation.navigate('orderDetail', plant)}>
              <Ionicons name="chevron-forward-outline" size={20} color="white" />
            </TouchableOpacity>
          </View>
        </View>
      </View>


    );
  };





  return (



    <SafeAreaView
      style={{ flex: 1, paddingHorizontal: 15, backgroundColor: COLORS.light,paddingVertical:10 }}>



      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.goBack()}>



          <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

        </TouchableOpacity>
        <View style={styles.searchBar}>
          <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: '50%' }}>My Order</Text>

        </View>

      </View>

<View>
      <FlatList


        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          marginTop: 10,
          paddingBottom: 50,
        }}

        data={MyOrderData}
        renderItem={({ item }) => {
          return <Card plant={item} />;
        }}
      />

</View>
    </SafeAreaView>


  )
}
export default CategoryItem;


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',

    marginBottom: '4%',

    // backgroundColor:COLORS.light

  },



  header: {
    marginTop: 30,
    flexDirection: 'row',

  },





  searchBar: {
    flexDirection: 'row',







  },
  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },
  badge: {
    position: 'absolute',
    backgroundColor: '#E6848C',
    width: 18,
    height: 18,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    right: -4,
    top: -4,
  },
  badgeText: {
    color: '#fff',
  },
  cart: {
    marginRight: 10
  },



  header: {
    // marginTop: 30,
    flexDirection: 'row',
    // justifyContent: 'space-between',
  },
  item: {
    flexDirection: 'row',
    marginTop: 8,

    shadowColor: "#000",
    textShadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    backgroundColor: 'white',
    elevation: 3,
    borderRadius: 10,

  },
  img: {
    height: 70,
    width: 70,
    // marginTop:"2%"
    justifyContent:'center',
    alignSelf:'center'
  },


});