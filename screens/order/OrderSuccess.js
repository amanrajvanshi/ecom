import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, TextInput, CheckBox, Image } from 'react-native';

import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';

import { useNavigation } from '@react-navigation/native';



const OrderSucess = () => {
    const navigation = useNavigation();




    return (



        <View style={{flex:1}} >
            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.navigate('checkout')}>


                    <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 25 }}>Order Success</Text>

                </View>
               
            </View>

            <View style={{ borderWidth: 0.5, borderColor: 'green', width: "90%", marginTop: 20, borderRadius: 5, flex:1, alignSelf: 'center', backgroundColor: '#8ebc0e',marginBottom:'2%' }}>
               <View style={{justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../images/order.png')} style={{ width: '70%', height: '50%'  }} />
                </View>
                <Text style={{ alignSelf: 'center', marginTop: 20, color: 'white', fontSize: 30, fontWeight: 'bold' }}>{`Your Order Was\n   Succesesful`}</Text>



                <Text style={{ alignSelf: 'center', marginTop: 20, color: 'white', fontSize: 18, fontWeight: 'bold' }}>{`Thanks for the order, you'll get a\n responce within a few minutes.`}</Text>

                <TouchableOpacity 
             
                    onPress={() => navigation.navigate('Track Order')}

                >
                       <View style={styles.buttonStyle}>
                    <Text style={{ color: 'white', alignSelf: 'center' , fontSize: 16, fontWeight: 'bold'}}>Track Order</Text>
                    </View>
                </TouchableOpacity>


            </View>

        </View>














    );

}







export default OrderSucess;

const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 8,
        marginRight: 8

    },





    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },

    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        backgroundColor: 'black',


        borderRadius: 10,
        // position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: "90%",
        height: "30%",
        alignSelf: 'center',
        marginTop: "8%"


    },
})