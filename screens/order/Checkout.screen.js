import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, TextInput, ScrollView, Button } from 'react-native';
import DatePicker from 'react-native-datepicker'
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';





export default class NumberScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = { date: "2016-05-15" }
    }

    render() {
        return (


            <View style={{flex:1}}>
                <View >
                    <View style={styles.container}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>


                            <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                        </TouchableOpacity>
                        <View style={styles.searchBar}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 40 }}>Checkout</Text>

                        </View>
                        {/* <View style={{  backgroundColor: 'orange', width: 40, height: 40, borderRadius: 150 / 2,alignSelf:'center',}}>
        <View style={{alignSelf:'center',marginTop:5}}>
         
          <Icon1 name="shoppingcart" size={30} color="white"  />
      
        </View>
        </View> */}
                    </View>

                </View>

                <ScrollView style={styles.scrollView}>
                    <View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderWidth: 1, borderColor: 'gray', backgroundColor: '#8ebc0e', marginLeft: 15, marginRight: 15, borderRadius: 5, height: 50 }}>

                            <Text style={{ marginLeft: 15, fontSize: 16, fontWeight: 'bold', marginTop: 13, color: 'white' }}>Order ID</Text>



                            <Text style={{ marginRight: 15, fontWeight: 'bold', marginTop: 13, color: 'white' }}>#0D2204</Text>




                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, height: 50, backgroundColor: '#F8F8F8', }}>

                            <Text style={{ marginLeft: 15, fontSize: 16, marginTop: 12 }}>Delivery Address</Text>



                            <Text style={{ marginRight: 15, marginTop: 12, fontSize: 16, }}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('DeliveryAddress')}>
                                    <Ionicons name="ellipsis-horizontal-outline" size={30} color="#222" backgroundColor="white" />
                                </TouchableOpacity>


                            </Text>







                        </View>

                        <View style={{ borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, height: 120, backgroundColor: '#F8F8F8' }} >
                            <Text style={{ marginTop: 10, marginLeft: 15, fontWeight: 'bold' }}>Home</Text>
                            <Text style={{ marginTop: 5, marginLeft: 15, }}>H.No 878, Huda Sector -13 Bhiwani{'\n'}Haryana 127021</Text>
                            <Text style={{ marginTop: 5, marginLeft: 15, }}> 7988382769</Text>


                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, height: 50, backgroundColor: '#F8F8F8', }}>

                            <Text style={{ marginLeft: 15, fontSize: 16, marginTop: 12 }}>Delivery Date</Text>











                        </View>
                        <View style={{ borderWidth: 0.5, borderColor: '#E0E0E0', height: 80, width: 360, alignSelf: 'center', backgroundColor: 'white' }}>
                            <DatePicker
                                style={{ width: 340, alignSelf: 'center', marginTop: 20 }}
                                date={this.state.date}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate="2016-05-01"
                                maxDate="2016-06-01"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => { this.setState({ date: date }) }}
                            />

                        </View>



                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, height: 50, backgroundColor: '#F8F8F8', }}>

                            <Text style={{ marginLeft: 15, fontSize: 16, marginTop: 12 }}>Order Bill</Text>



                            <Text style={{ marginRight: 15, marginTop: 6, fontSize: 16, }}>



                            </Text>







                        </View>


                        <View style={{ borderWidth: 0.5, borderColor: 'gray', marginLeft: 16, marginRight: 16, backgroundColor: 'white' }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>

                                <Text style={{ marginLeft: 35, fontSize: 16 }}>Item</Text>



                                <Text style={{ marginRight: 35 }}>4</Text>




                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

                                <Text style={{ marginLeft: 35, fontSize: 16 }}>Sub Total</Text>



                                <Text style={{ marginRight: 35 }}>$100.00</Text>




                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

                                <Text style={{ marginLeft: 35, fontSize: 16 }}>Delivery Charge</Text>



                                <Text style={{ marginRight: 35 }}>Free</Text>




                            </View>



                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

                                <Text style={{ marginLeft: 35, fontSize: 16 }}>Discount</Text>



                                <Text style={{ marginRight: 35 }}>$10.0</Text>




                            </View>


                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginBottom: 20, borderTopWidth: 1, borderTopColor: '#E0E0E0' }}>

                                <Text style={{ marginLeft: 35, fontSize: 16, fontWeight: 'bold', marginTop: 10 }}>Total</Text>



                                <Text style={{ marginRight: 35, fontWeight: 'bold', marginTop: 10 }}>$100.00</Text>

                            </View>

                        </View>

                    </View>

                </ScrollView>
                <View>
                <TouchableOpacity style={styles.buttonStyle}
                    onPress={() => this.props.navigation.navigate('payment')}

                >
                    <Text style={{ color: 'white', fontWeight: 'bold' }}>CONTINUE</Text>
                </TouchableOpacity>

                </View>

            </View>




        );

    }


}











const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 11,
        marginRight: 11

    },

    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },

    scrollView: {

        // marginBottom: 140,
        // marginTop: 20,
    },

    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },

    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        backgroundColor: '#8ebc0e',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        width: 375,
        height: 50,
        alignSelf: 'center',
        marginTop:'3%',
        marginBottom:'2%'
      


    },


    butn1: {
        backgroundColor: 'white',



        position: 'absolute',
        justifyContent: 'center',

        width: 330,
        height: 45,
        alignSelf: 'center',
        marginTop: 18,
        borderRadius: 8


    },

    butn2: {
        backgroundColor: 'white',



        // position: 'absolute',
        justifyContent: 'center',

        width: 330,
        height: 45,
        alignSelf: 'center',
        marginTop: 6,
        borderRadius: 8


    }
})