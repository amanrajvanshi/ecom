import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, TextInput, CheckBox } from 'react-native';

import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
// import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';
import { RadioButton } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';



const DeliveryAddress = () => {
    const navigation = useNavigation();
    const [checked, setChecked] = React.useState('first');



    return (


        <View style={{flex:1}}>
            <View >
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>


                        <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white" />

                    </TouchableOpacity>
                    <View style={styles.searchBar}>
                        <Text style={{ fontSize: 19, fontWeight: 'bold', }}>Delivery Address</Text>

                    </View>
                 
                </View>

            </View>
            <View style={{ marginLeft: 25, marginTop: 20, }}>

                <Text>
                    Shipping Address
            </Text>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, height: 140, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, backgroundColor: '#F8F8F8', }}>
                <Text style={{ marginLeft: 15, marginTop:40, fontSize: 16, }}>
                <RadioButton
        value="first"
        status={ checked === 'first' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('first')}
        color={"orange"}
      />
                </Text>
                <View>

                    <Text style={{ marginTop: 10, fontWeight: 'bold' }}>

                        Home</Text>
                    <Text style={{ marginTop: 5, }}>H.No 878, Huda Sector -13 Bhiwani{'\n'}Haryana 127021</Text>
                    <Text style={{ marginTop: 5, fontWeight: 'bold' }}>Mobile: 7988382769</Text>
                </View>



                <Text style={{ marginRight: 15, marginTop: 12, fontSize: 16, }}>
                    <TouchableOpacity onPress={() => navigation.navigate('AddNewAdrs')}>
                        <Ionicons name="ellipsis-horizontal-outline" size={30} color="#222" backgroundColor="white" />
                    </TouchableOpacity>


                </Text>







            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, height: 140, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, backgroundColor: '#F8F8F8', }}>
                <Text style={{ marginLeft: 15, marginTop: 40, fontSize: 16, }}>
                <RadioButton
        value="second"
        status={ checked === 'second' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('second')}
        color={"orange"}
      />
                </Text>
                <View>

                    <Text style={{ marginTop: 10, fontWeight: 'bold' }}>

                        Home</Text>
                    <Text style={{ marginTop: 5, }}>H.No 878, Huda Sector -13 Bhiwani{'\n'}Haryana 127021</Text>
                    <Text style={{ marginTop: 5, fontWeight: 'bold' }}>Mobile: 7988382769</Text>
                </View>



                <Text style={{ marginRight: 15, marginTop: 12, fontSize: 16, }}>
                    <TouchableOpacity onPress={() => navigation.navigate('AddNewAdrs')}>
                        <Ionicons name="ellipsis-horizontal-outline" size={30} color="#222" backgroundColor="white" />
                    </TouchableOpacity>


                </Text>







            </View>
              
            <TouchableOpacity 
                onPress={() => navigation.navigate('checkout')}

            >
                <View style={styles.buttonStyle}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>CONTINUE</Text>
                </View>
            </TouchableOpacity>


        </View>








    );

}







export default DeliveryAddress;

const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 8,
        marginRight: 8

    },





    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        backgroundColor: '#8ebc0e',

        marginTop: '30%',
        borderRadius: 10,
        // position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: "90%",
        height: "20%",
        alignSelf: 'center',


    },
})