import React from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,
    ScrollView,

    TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import { ScrollView } from 'react-native-gesture-handler';
import StarRating from 'react-native-star-rating';

function OrderDetail({ navigation, route }) {
    const plant = route.params;
    return (
        <SafeAreaView
            style={{ flex: 1, backgroundColor: COLORS.light, paddingHorizontal: 0 }}>



            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.goBack()}>



                    <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: '30%' }}>Order Detail</Text>

                </View>

            </View>

            <ScrollView >

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderWidth: 1, borderColor: 'gray', backgroundColor: '#8ebc0e', marginLeft: 15, marginRight: 15, borderRadius: 5, height: 50 }}>

                    <Text style={{ marginLeft: 15, fontSize: 16, fontWeight: 'bold', marginTop: 13, color: 'white' }}>Order ID</Text>



                    <Text style={{ marginRight: 15, fontWeight: 'bold', marginTop: 13, color: 'white' }}>#0D2204</Text>




                </View>
                <View style={styles.item}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Image source={plant.img} resizeMode="contain" style={styles.img} />
                        <View style={{ flexDirection: 'column', justifyContent: 'center', paddingRight: 20, marginTop: 9 }}>

                            <Text style={{ fontWeight: 'bold', fontSize: 15 }}>{plant.name}</Text>
                            <Text >Rs {plant.price}</Text>
                            <Text style={{ color: 'gray', fontWeight: 'bold' }}>{plant.weight}</Text>

                        </View>
                    </View>
                </View>

                <View style={styles.item}>
                    <View style={{ paddingHorizontal: 10, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text
                            style={{
                                color: 'grey',
                                fontSize: 16,
                                lineHeight: 22,
                                fontWeight: 'bold',
                                // marginTop: 10,

                            }}>
                            Rate  Product
                        </Text>
                        <View style={{ marginLeft: 10, }}>
                            <StarRating
                                disabled={false}
                                emptyStar={'ios-star-outline'}
                                fullStar={'ios-star'}
                                halfStar={'ios-star-half'}
                                iconSet={'Ionicons'}
                                maxStars={5}


                                fullStarColor={'orange'}
                                starSize={20}





                            />
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, height: 50, backgroundColor: 'White', }}>

                    <Text style={{ marginLeft: 15, fontSize: 16, marginTop: 12, fontWeight: 'bold' }}>Shipping  Address</Text>









                </View>

                <View style={{ borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, height: 120, backgroundColor: 'white' }} >
                    <Text style={{ marginTop: 10, marginLeft: 15, fontWeight: 'bold' }}>Amit</Text>
                    <Text style={{ marginTop: 5, marginLeft: 15, }}>H.No 878,{'\n'}Huda Sector -13 Bhiwani{'\n'}Haryana 127021</Text>
                    <View style={{flexDirection:'row'}}>
                    <Text style={{ marginTop: 5, marginLeft: 15,fontWeight:'bold' }}>Phone no.</Text>
                    <Text style={{ marginTop: 5, marginLeft: 15, }}>7988382769,7988382769</Text>
                    </View>
                    


                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, height: 50, backgroundColor: '#F8F8F8', }}>

                    <Text style={{ marginLeft: 15, fontSize: 16, marginTop: 12,fontWeight:'bold' }}>Price Detail</Text>



                    <Text style={{ marginRight: 15, marginTop: 6, fontSize: 16, }}>



                    </Text>







                </View>
                <View></View>

                <View style={{ borderWidth: 0.5, borderColor: 'gray', marginLeft: 18, marginRight: 18, backgroundColor: 'white', }}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingHorizontal: 20 }}>

                        <Text style={{ fontSize: 16 }}>Item</Text>



                        <Text style={{}}>4</Text>




                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, paddingHorizontal: 20 }}>

                        <Text style={{ fontSize: 16 }}>Sub Total</Text>



                        <Text style={{}}>Rs 100.00</Text>




                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, paddingHorizontal: 20 }}>

                        <Text style={{ fontSize: 16 }}>Delivery Charge</Text>



                        <Text style={{}}>Free</Text>




                    </View>



                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, paddingHorizontal: 20 }}>

                        <Text style={{ fontSize: 16 }}>Discount</Text>



                        <Text style={{}}>Rs 10.0</Text>




                    </View>


                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginBottom: 20, borderTopWidth: 1, borderTopColor: '#E0E0E0', paddingHorizontal: 20 }}>

                        <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 10 }}>Total</Text>



                        <Text style={{ fontWeight: 'bold', marginTop: 10 }}>Rs 100.00</Text>




                    </View>






                </View>
            </ScrollView>










        </SafeAreaView>

    )
}
export default OrderDetail;



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: '4%',
        marginLeft: '2%'
        // marginTop: 5,






    },



    searchBar: {
        flexDirection: 'row',
        flex: 1,

    },



    item: {
        flex: 1,

        marginTop: 8,
        padding: 8,
        shadowColor: "#000",
        textShadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        backgroundColor: 'white',
        elevation: 3,
        borderRadius: 5,
        marginLeft: 15,
        marginRight: 14



    },
    img: {
        height: 70,
        width: 70
    },





});