import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, TextInput, CheckBox } from 'react-native';

import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
// import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';

import { useNavigation } from '@react-navigation/native';
// import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox';  
import { RadioButton } from 'react-native-paper';



const PaymentScreen = () => {
    const navigation = useNavigation();
    const [checked, setChecked] = React.useState('first');



    return (


        <View>
            <View >
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => navigation.navigate('checkout')}>


                        <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white" />

                    </TouchableOpacity>
                    <View style={styles.searchBar}>
                        <Text style={{ fontSize: 22, fontWeight: 'bold', }}>Payment</Text>

                    </View>
                   
                </View>

            </View>
            <View style={{ marginLeft: 25, marginTop: 20, }}>

                <Text>
                    All Payment Options
            </Text>
            </View>

            <View style={{ flexDirection: 'row', marginTop: 20, height: 70, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, backgroundColor: '#F8F8F8', }}>
                <Text style={{ marginLeft: 15, marginTop: 22, fontSize: 16, }}>
                <RadioButton
        value="first"
        status={ checked === 'first' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('first')}
        color={"orange"}
      />
                </Text>
                <View>

                    <Text style={{ marginTop: 22, fontWeight: 'bold', marginLeft: 20 }}>

                        UPI</Text>

                </View>

            </View>






        




            <View style={{ flexDirection: 'row', marginTop: 10, height: 70, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, backgroundColor: '#F8F8F8', }}>
                <Text style={{ marginLeft: 15, marginTop: 22, fontSize: 16, }}>
                <RadioButton
        value="second"
        status={ checked === 'second' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('second')}
        color={"orange"}
      />
                </Text>
                <View>

                    <Text style={{ marginTop: 22, fontWeight: 'bold', marginLeft: 20 }}>

                        Add Debit/Credit/ATM Card</Text>

                </View>

            </View>


            <View style={{ flexDirection: 'row', marginTop: 10, height: 70, borderWidth: 1, borderColor: '#E0E0E0', marginLeft: 15, marginRight: 15, backgroundColor: '#F8F8F8', }}>
                <Text style={{ marginLeft: 15, marginTop: 22, fontSize: 16, }}>
                <RadioButton
        value="third"
        status={ checked === 'third' ? 'checked' : 'unchecked' }
        onPress={() => setChecked('third')}
        color={"orange"}
      />
                </Text>
                <View>

                    <Text style={{ marginTop: 22, fontWeight: 'bold', marginLeft: 20 }}>

                        Pay On Delivery</Text>

                </View>

            </View>




<View style={styles.buttonbackStyle}>
            <TouchableOpacity style={styles.buttonStyle}
                onPress={() => navigation.navigate('Order Success')}

            >
                <Text style={{ color: 'white', fontWeight: 'bold',marginTop:15 }}>CONTINUE</Text>
            </TouchableOpacity>



            </View>





        </View>










    );

}







export default PaymentScreen;

const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 8,
        marginRight: 8

    },





    searchBar: {
        flexDirection: 'row',
        flex: 1,

        paddingHorizontal: 95,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center',

    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonbackStyle: {
        backgroundColor: '#8ebc0e',

        marginTop: '20%',
        borderRadius: 10,
      alignContent:'center',
        alignItems: 'center',
        width: 365,
        height: 50,
        alignSelf: 'center',


    },
})