import React from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,

    TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Searchbar } from 'react-native-paper'
function SupportScreen({ navigation }) {



    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);

    return (
        <SafeAreaView
            style={{ flex: 1, backgroundColor: COLORS.light, paddingHorizontal: 20 }}>

            {/* header  */}

            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.navigate('profile')}>



                    <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Support Center</Text>

                </View>

            </View>


            {/* searchbar  */}





            {/* support center green content  */}



            <View style={styles.backgroundImageContainer}>

                <View style={styles.backgroundImage}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                        <View style={{}}>
                            <Image
                                source={require('../../assets/support.png')}
                                style={{
                                    width: 120, height: 120,
                                }}
                            />
                        </View>
                        <View style={{ flexDirection: 'column', marginTop: 15 }}>
                            <View style={styles.name}>
                                <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: 'white', fontSize: 30 }}>Live Chat</Text>
                                <Text style={{ fontSize: 13, fontWeight: 'bold', color: 'white', alignSelf: 'center', marginLeft: -17 }}>With Our Support</Text>
                                <View style={{ backgroundColor: 'black', borderRadius: 100, width: 70, marginTop: 5 }}>
                                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'white', alignSelf: 'center', }}>
                                        Start</Text></View>
                            </View>
                        </View>
                    </View>

                </View>


            </View>




            <View style={styles.detailsContainer}>

                <View
                    style={{
                        // marginLeft: 10,
                        backgroundColor: COLORS.light,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderTopColor: 'transparent',
                        borderLeftColor: 'transparent',
                        borderRightColor: 'transparent',
                        borderBottomColor: 'lightgrey',
                        borderBottomWidth: 1,
                        borderTopEndRadius: 10,
                        borderTopLeftRadius: 10,
                        //  borderColor: "#4d4d4d",
                        shadowColor: "#4d4d4d",
                        shadowOffset: {
                            width: 0,
                            height: 8,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 13.51,
                        elevation: 5,
                        // paddingBottom:30,
                        marginBottom: 30,
                        // paddingHorizontal:10
                        // 



                    }}>
                    {/* card header  */}

                    <View style={{ flexDirection: 'row', paddingVertical: 10, paddingHorizontal: 10 }}>
                        <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                            <Icon name="help" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                        </View>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 10 }}>Frequently Asked Questions</Text>
                    </View>

                </View>

                {/* card content  */}


                <View style={{ paddingHorizontal: 20, marginTop: -13, flexDirection: 'row', }}>

                    <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                        <Icon name="email" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                    </View>


                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft: 10
                        }}>
                        My Order didn't recived yet

                            </Text>


                </View>




                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>


                    <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                        <Icon name="phone" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                    </View>


                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft: 10
                        }}>
                        +51879565264

                            </Text>



                </View>



                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>


                    <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                        <Icon name="email" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                    </View>

                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft: 10
                        }}>
                        Change my phone number

                            </Text>



                </View>


                {/* 

                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>


                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft: 10
                        }}>
                     Change my delivery address

                                </Text>

                    <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                        <Ionicons name="chevron-forward-outline" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                    </View>





                </View> */}

            </View>







        </SafeAreaView>

    )
}
export default SupportScreen;



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        // marginTop: 5,
        marginHorizontal: -13,
        paddingBottom: 17,
        // paddingHorizontal: 20,   
        // backgroundColor:COLORS.light



    },



    searchBar: {
        flexDirection: 'row',
        flex: 1,

        paddingHorizontal: 40,
        paddingVertical: 10,

        marginHorizontal: 62,

    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    detailsContainer: {
        flex: 1,
        backgroundColor: COLORS.light,

        borderRadius: 10,
        borderWidth: 1,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'lightgrey',

        shadowColor: "#4d4d4d",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.8,
        shadowRadius: 13.51,
        elevation: 5,
        marginBottom: 200,



    },


    addCartBtnBackground: {
        alignSelf: 'center',
        // marginTop:-35,
        marginBottom: 20,

        width: 150,
        height: 100,

        justifyContent: 'center',
        alignItems: 'center',

    },
    addCartBtn: {
        backgroundColor: 'white',
        borderRadius: 20,

        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 380,
        height: 60,
        alignSelf: 'center',
        backgroundColor: '#80B026',
        borderWidth: 1

    },
    backgroundStyle: {
        marginTop: 10,
        backgroundColor: '#FFFBF2',
        height: 50,
        width: '71%',
        borderRadius: 30,
        // marginHorizontal:10,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20
    },
    textInput: {

        flex: 1,
        fontSize: 15

    },
    iconStyle: {
        fontSize: 25,
        alignSelf: 'center',
        color: '#878582'

    },
    backgroundImageContainer: {
        alignItems: 'center',
        height: 150,
        // marginHorizontal: 10,
        borderRadius: 20
    },
    backgroundImage: {
        height: '80%',
        width: '100%',
        overflow: 'hidden',
        // top: 40,
        borderRadius: 10,
        backgroundColor: '#8AC02A'

    },
    name: {
        // marginHorizontal: 80,
        // marginVertical: 85
    }


});