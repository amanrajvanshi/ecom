import React from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,

    TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

function SettingScreen({ navigation }) {
    return (
        <SafeAreaView
            style={{ flex: 1, paddingHorizontal: 20, backgroundColor: COLORS.light }}>



            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.navigate('Home')}>



                    <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Setting</Text>

                </View>

            </View>


            <View style={styles.detailsContainer}>
                <View
                    style={{
                        // marginLeft: 10,
                        backgroundColor: COLORS.light,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderTopColor: 'transparent',
                        borderLeftColor: 'transparent',
                        borderRightColor: 'transparent',
                        borderBottomColor: 'lightgrey',
                        borderBottomWidth: 1,
                        borderTopEndRadius: 10,
                        borderTopLeftRadius: 10,
                        //  borderColor: "#4d4d4d",
                        shadowColor: "#4d4d4d",
                        shadowOffset: {
                            width: 0,
                            height: 8,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 13.51,
                        elevation: 5,
                        // paddingBottom:30,
                        marginBottom: 30



                    }}>
                    {/* card header  */}

                    <View style={{ flexDirection: 'row', paddingVertical: 10, paddingHorizontal: 10 }}>
                        <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                            <Icon name="account" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                        </View>
                        <Text style={{ fontSize: 22, fontWeight: 'bold', marginLeft: 10 }}>Account</Text>
                    </View>

                </View>

                {/* card content  */}


                <View style={{ paddingHorizontal: 20, marginTop: -13, flexDirection: 'row', justifyContent: 'space-between' }}>

                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft: 10
                        }}>
                        Edit Profile

                            </Text>

                    <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                        <TouchableOpacity onPress={() => navigation.navigate('newadd')}>
                            <Ionicons name="chevron-forward-outline" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                        </TouchableOpacity>
                    </View>

                </View>



                {/* 
                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent:'space-between' }}>


                <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                             Change Password

                            </Text>

                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                    <Ionicons name="chevron-forward-outline" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
               




                </View> */}



                {/* <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent:'space-between' }}>


                <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                               Privacy settings

                            </Text>

                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                    <Ionicons name="chevron-forward-outline" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
               




                </View> */}




                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>


                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft: 10
                        }}>
                        SignOut

                                </Text>

                    <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29 }}>
                        <TouchableOpacity onPress={() => navigation.navigate('newadd')}>
                            <Ionicons name="chevron-forward-outline" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                        </TouchableOpacity>
                    </View>





                </View>

            </View>


            {/* 
            <View style={styles.detailsTwoContainer}>
                <View
                    style={{
                        // marginLeft: 10,
                        backgroundColor: COLORS.light,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderTopColor: 'transparent',
                        borderLeftColor: 'transparent',
                        borderRightColor: 'transparent',
                        borderBottomColor: 'lightgrey',
                        borderBottomWidth: 1,
                        borderTopEndRadius: 10,
                        borderTopLeftRadius: 10,
                        //  borderColor: "#4d4d4d",
                        shadowColor: "#4d4d4d",
                        shadowOffset: {
                            width: 0,
                            height: 8,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 13.51,
                        elevation: 5,
                        // paddingBottom:30,
                        marginBottom:30


                        
                    }}> */}
            {/* card header  */}

            {/* <View style={{ flexDirection: 'row',paddingVertical:10,paddingHorizontal:10 }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                    <Icon name="perspective-more" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                     </View>
                        <Text style={{ fontSize: 22, fontWeight: 'bold',  marginLeft: 10 }}>More</Text>
                    </View>
               
                </View> */}

            {/* card content  */}


            {/* <View style={{ paddingHorizontal: 20, marginTop: -13, flexDirection: 'row',justifyContent:'space-between' }}>

                <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                      Language

                            </Text>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                    <Ionicons name="chevron-forward-outline" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                    </View>

                </View>




                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent:'space-between' }}>


                <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                           Currency

                            </Text>

                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                    <Ionicons name="chevron-forward-outline" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
               




                </View>



                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent:'space-between' }}>


                <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                          Country

                            </Text>

                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                    <Ionicons name="chevron-forward-outline" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
               




                </View>




                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent:'space-between' }}>


                            <Text
                           style={{
                                 color: 'grey',
                                 fontSize: 15,
                                 lineHeight: 22,
                                 marginTop: 5,
                                   fontWeight: 'bold',
                                   marginLeft:10
                                     }}>
                              Linked Account

                                </Text>

                   <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                   <Ionicons name="chevron-forward-outline" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                   </View>





                       </View>
                
            </View> */}



        </SafeAreaView>

    )
}
export default SettingScreen;



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        // marginTop: 5,
        marginHorizontal: -13,
        paddingBottom: 17,
        // backgroundColor:COLORS.light



    },



    searchBar: {
        flexDirection: 'row',
        flex: 1,

        paddingHorizontal: 80,
        paddingVertical: 10,

        marginHorizontal: 50,

    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    detailsContainer: {
        flex: 0.35,
        backgroundColor: COLORS.light,
        marginHorizontal: -7,
        height: 60,
        // marginBottom: 190,
        borderRadius: 10,
        // marginBottom:70,
        // paddingTop: 10,
        borderWidth: 1,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
        borderTopEndRadius: 10,
        borderTopLeftRadius: 10,
        //  borderColor: "#4d4d4d",
        shadowColor: "#4d4d4d",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.8,
        shadowRadius: 13.51,
        elevation: 5,

    },
    detailsTwoContainer: {
        flex: 1,
        backgroundColor: COLORS.light,
        marginHorizontal: -7,
        marginTop: 40,
        // marginBottom: 200,
        borderRadius: 10,
        // top: -20,
        // paddingBottom: 100,
        marginBottom: 200,
        borderWidth: 1,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
        borderTopEndRadius: 10,
        borderTopLeftRadius: 10,
        //  borderColor: "#4d4d4d",
        shadowColor: "#4d4d4d",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.8,
        shadowRadius: 13.51,
        elevation: 5,

    },

    addCartBtnBackground: {
        alignSelf: 'center',
        // marginTop:-35,
        marginBottom: 20,

        width: 150,
        height: 100,

        justifyContent: 'center',
        alignItems: 'center',

    },
    addCartBtn: {
        backgroundColor: 'white',
        borderRadius: 20,

        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 380,
        height: 60,
        alignSelf: 'center',
        backgroundColor: '#80B026',
        borderWidth: 1

    }


});