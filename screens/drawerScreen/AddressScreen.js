import React from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,

    TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

function AddressScreen({ navigation }) {
    return (
        <SafeAreaView
            style={{ flex: 1, paddingHorizontal: 20, backgroundColor: COLORS.light }}>



            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.navigate('profile')}>



                    <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>My Address</Text>

                </View>
               
            </View>


            <View style={styles.detailsContainer}>
                <View
                    style={{
                        // marginLeft: 10,
                        backgroundColor: COLORS.light,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderTopColor: 'transparent',
                        borderLeftColor: 'transparent',
                        borderRightColor: 'transparent',
                        borderBottomColor: 'lightgrey',
                        borderBottomWidth: 1,
                        borderTopEndRadius: 10,
                        borderTopLeftRadius: 10,
                        //  borderColor: "#4d4d4d",
                        shadowColor: "#4d4d4d",
                        shadowOffset: {
                            width: 0,
                            height: 8,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 13.51,
                        elevation: 5,
                        // paddingBottom:30,
                        marginBottom:30


                        
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name="home" size={40}style={{color:COLORS.orange, marginLeft: 15}} />
                        <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: 8, marginLeft: 10 }}>Home</Text>
                    </View>
                    <View style={{
                        backgroundColor: '#EEFACA',
                        width: 80,
                        height: 14,

                        alignSelf: 'center',
                        marginRight: 20,
                        top: -20

                    }}>
                        <Text style={{ color: 'green', fontWeight: 'bold', alignSelf: 'center',  }}>Default</Text>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 20, marginTop: -13, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="account" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                        Amit Patel

                            </Text>




                </View>



                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="phone" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                             + 565645465454 

                            </Text>




                </View>

                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="email" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 7,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                        amitpatel005577@gmail.com

                            </Text>




                </View>

                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="map-marker" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            // marginTop: 7,
                            fontWeight: 'bold',
                            marginLeft:10,
                            paddingRight:20
                            

                    
                        }}>
                        Amit Patel lkfajsdklfjkjfkajsfdkljklsdjfkljalfj

                            </Text>




                </View>
                
            </View>





            <View style={styles.detailsTwoContainer}>
                <View
                    style={{
                        // marginLeft: 10,
                        backgroundColor: COLORS.light,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderTopColor: 'transparent',
                        borderLeftColor: 'transparent',
                        borderRightColor: 'transparent',
                        borderBottomColor: 'lightgrey',
                        borderBottomWidth: 1,
                        borderTopEndRadius: 10,
                        borderTopLeftRadius: 10,
                        //  borderColor: "#4d4d4d",
                        shadowColor: "#4d4d4d",
                        shadowOffset: {
                            width: 0,
                            height: 8,
                        },
                        shadowOpacity: 0.8,
                        shadowRadius: 13.51,
                        elevation: 5,
                        // paddingBottom:30,
                        marginBottom:30


                        
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name="office-building" size={40} style={{color:COLORS.parat, marginLeft: 15}}/>
                        <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: 8, marginLeft: 10 }}>Home</Text>
                    </View>
                    {/* <View style={{
                        backgroundColor: '#EEFACA',
                        width: 80,
                        height: 24,

                        alignSelf: 'center',
                        marginRight: 20,
                        top: -16

                    }}>
                        <Text style={{ color: 'green', fontWeight: 'bold', alignSelf: 'center' }}>Default</Text>
                    </View> */}
                </View>
                <View style={{ paddingHorizontal: 20, marginTop: -13, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="account" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                        Amit Patel

                            </Text>




                </View>



                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="phone" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                        Amit Patel

                            </Text>




                </View>

                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="email" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 5,
                            fontWeight: 'bold',
                            marginLeft:10
                        }}>
                        Amit Patel

                            </Text>




                </View>

                <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', }}>
                    <View style={{backgroundColor:COLORS.white,borderRadius:100,width:29,height:29}}>
                <Icon name="map-marker" size={25} style={{alignSelf:'center',color:COLORS.orange}} />
                </View>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 15,
                            lineHeight: 22,
                            // marginTop: -5,
                            fontWeight: 'bold',
                            marginLeft:10,
                            paddingRight:20
                        }}>
                        village kiratpur post danpur Rudrapur Ut udham singh navig
                     

                            </Text>




                </View>
                
            </View>






        </SafeAreaView>

    )
}
export default AddressScreen;



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        // marginTop: 5,
        marginHorizontal: -13,
        paddingBottom: 17     ,
         // backgroundColor:COLORS.light
   


    },



    searchBar: {
        flexDirection: 'row',
        flex: 1,

        paddingHorizontal: 80,
        paddingVertical: 10,

        marginHorizontal: 10,

    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    detailsContainer: {
        flex: 1,
        backgroundColor: COLORS.light,
        marginHorizontal: -7,
        marginBottom: 47,
        borderRadius: 10,
        // top: -30,
        // paddingTop: 10,
        borderWidth: 1,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
        borderTopEndRadius: 10,
        borderTopLeftRadius: 10,
        //  borderColor: "#4d4d4d",
        shadowColor: "#4d4d4d",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.8,
        shadowRadius: 13.51,
        elevation: 5,
        
    },
    detailsTwoContainer:{
        flex: 1,
        backgroundColor: COLORS.light,
        marginHorizontal: -7,
        // marginBottom: 47,
        borderRadius: 10,
        top: -20,
        paddingBottom: 10,
        borderWidth: 1,
        borderTopColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 1,
        borderTopEndRadius: 10,
        borderTopLeftRadius: 10,
        //  borderColor: "#4d4d4d",
        shadowColor: "#4d4d4d",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.8,
        shadowRadius: 13.51,
        elevation: 5,

    },
    
    addCartBtnBackground: {
        alignSelf: 'center',
        // marginTop:-35,
        marginBottom:20,
        
        width: 150,
        height: 100,
        // borderColor: 'grey',
        // 
        // borderWidth: 1,
        // borderBottomLeftRadius: 5,
        // borderBottomRightRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',

    },
    addCartBtn: {
        backgroundColor: 'white',
        borderRadius:10,
        // marginTop: 10,
        // borderBottomLeftRadius: 5,
        // borderBottomRightRadius: 5,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 360,
        height: 60,
        alignSelf: 'center',
        backgroundColor: '#80B026',
        borderWidth: 1,
        

    }


});