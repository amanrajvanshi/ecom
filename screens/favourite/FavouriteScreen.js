import React, { component } from 'react'
import { View, Text, StyleSheet, SafeAreaView, FlatList, Image, Alert, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';


import Swipeout from 'react-native-swipeout';
import NumericInput from 'react-native-numeric-input'
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
const ic_delete = require('../images/delete.png');

export default class FavouriteScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [
                {
                    id: 0, price: '$35.0 x2', name: 'Tomato', quantity: '1kg', img: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Tomato_je.jpg'
                },

                {
                    id: 1, price: '$35.0 x2', name: 'potato', quantity: '1kg', img: 'https://cdn.britannica.com/89/170689-131-D20F8F0A/Potatoes.jpg'
                },

                {
                    id: 2, price: '$35.0 x2', name: 'ladyfinger', quantity: '1kg', img: 'https://mastomart.in/wp-content/uploads/2020/08/Red-Onion.jpg'
                },
                {
                    id: 3, price: '$35.0 x2', name: 'Tomato', quantity: '1kg', img: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Tomato_je.jpg'
                },

                {
                    id: 4, price: '$35.0 x2', name: 'potato', quantity: '1kg', img: 'https://cdn.britannica.com/89/170689-131-D20F8F0A/Potatoes.jpg'
                },

                {
                    id: 5, price: '$35.0 x2', name: 'ladyfinger', quantity: '1kg', img: 'https://mastomart.in/wp-content/uploads/2020/08/Red-Onion.jpg'
                },
                {
                    id: 6, price: '$35.0 x2', name: 'Tomato', quantity: '1kg', img: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Tomato_je.jpg'
                },
                {
                    id: 7, price: '$35.0 x2', name: 'Tomato', quantity: '1kg', img: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Tomato_je.jpg'
                },




            ]

        }
    }

    item = null;

    _renderButtonDelete = () => {
        return (
            <View style={{borderRadius:10}}>
                <TouchableOpacity style={styles.button} onPress={
                    () => {
                        this.setState({
                            data: [... this.state.data.filter(e => e.id != this.item.id)]
                        })
                        this.item = null
                    }
                }>
                <View style={{marginBottom:120,borderRadius:100}}>
                <Ionicons name="trash" size={40}  color = "white" ></Ionicons>
                </View>
                 
                </TouchableOpacity>
            </View>

        )
    }
    SwipeoutBtns = [{
        // text: 'Delete',
        component: this._renderButtonDelete(),
        backgroundColor: 'orange',
        borderRadius:100,








    },

    ];

    renderItem = ({ item, index }) => {
        return (

            <Swipeout
                onOpen={
                    () => {
                        this.item = item
                    }
                }
                style={{ padding: 4, backgroundColor: '#F0F0F0' }}
                autoClose
                right={this.SwipeoutBtns}>

                <View style={styles.item}>
                    <Image source={{ uri: item.img }} resizeMode="contain" style={styles.img} />
                    <View style={{ flexDirection: 'column', marginLeft: 30, flex: 1, justifyContent: 'center' }}>

                        <Text style={{ fontWeight: 'bold', fontSize: 15 }}>{item.name}</Text>
                        <Text >{item.price}</Text>
                        <Text style={{ color: 'gray', fontWeight: 'bold' }}>{item.quantity}</Text>


                    </View>
                    <View style={{ backgroundColor: 'orange', width: 40, height: 40, borderRadius: 150 / 2, marginTop: 15, marginRight: 13 }}>
                        <View style={{ marginTop: 10, alignSelf: 'center' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Order')}>
                                <Icon1 name="shoppingcart" size={20} color="white" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Swipeout>

        )
    }

    render() {
        const { data } = this.state;
        return (
            <SafeAreaView

            >



                <View style={styles.container}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>


                        <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                    </TouchableOpacity>
                    <View style={styles.searchBar}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: "40%" }}>My Favourite</Text>

                    </View>


                </View>
                <View>

                    <FlatList
                        style={styles.List}
                        data={data}
                        keyExtractor={(item, index) => item.id}
                        renderItem={this.renderItem}
                        extraData={data}
                    />








                </View>


            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({



    List: {
        paddingHorizontal: '3%',

    },
    item: {
        flexDirection: 'row',
        marginTop: '-1%',
        marginBottom: '0.5%',
        padding: 8,
        shadowColor: "#000",
        textShadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        backgroundColor: 'white',
        elevation: 3,
        borderRadius: 10,

    },

    img: {
        height: 70,
        width: 70
    },
    button: {
        justifyContent: 'center', alignItems: 'center', marginTop: 30
        , padding: 12
    },

    ImageText: {
        position: 'absolute',
        color: 'white',
        marginTop: 4,
        fontSize: 14,
        left: 5,
        bottom: 10,
        padding: 5,
        backgroundColor: 'black',
        // borderWidth:2,
        borderRadius: 5,



    },
    container: {
        flexDirection: 'row',
        paddingBottom: '3%',
        marginTop: 5,
        paddingHorizontal: 10,
        // justifyContent:'space-around'

        // marginLeft: 13,
        // marginRight: 13,
        // alignSelf:'center'




    },

    searchBar: {
        // flexDirection: 'row',
        alignSelf: 'center',
        // flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,


        fontSize: 50,

    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        backgroundColor: '#8ebc0e',

        marginTop: 10,
        borderRadius: 10,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 350,
        height: 50,
        alignSelf: 'center',


    },
})