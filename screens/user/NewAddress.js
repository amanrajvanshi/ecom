import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, Label, TextInput, CheckBox, Image } from 'react-native';

import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';

import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';



const NewAdrs = () => {
    const navigation = useNavigation();




    return (



        <View >
            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.goBack()}>


                    <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 40 }}>Edit Profile</Text>

                </View>

            </View>

            <ScrollView style={{ marginBottom: 160, marginTop: 20 }}>
                <View>
                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Name</Text>
                        <TextInput

                            placeholder="Enter Your Name"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ marginTop: 20, borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Email Address</Text>
                        <TextInput
                            textContentType='emailAddress'
                            placeholder="Enter Your Email"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>

                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Phone No.</Text>
                        <TextInput
                            textContentType='telephoneNumber'
                            keyboardType='number-pad'

                            maxLength={10}

                            placeholder="Enter Your Phone No"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>






                    {/* <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Name</Text>
                        <TextInput

                            placeholder="Lucy martin"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View>
                    <View style={{ borderWidth: 0.5, borderColor: 'gray', height: 60, width: 365, alignSelf: 'center', borderRadius: 8, marginTop: 10 }}>
                        <Text style={{ marginTop: 7, marginLeft: 15, color: 'gray' }}>Name</Text>
                        <TextInput

                            placeholder="Lucy martin"
                            style={{ marginLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

                        />
                    </View> */}
                </View>

            </ScrollView>


            <TouchableOpacity style={styles.buttonStyle}
                onPress={() => navigation.goBack()}

            >
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Save Profile</Text>
            </TouchableOpacity>

        </View>














    );

}







export default NewAdrs;

const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 8,
        marginRight: 8

    },





    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },

    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        backgroundColor: '#8ebc0e',


        borderRadius: 10,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 365,
        height: 50,
        alignSelf: 'center',
        marginTop: 300


    },
})