import React from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,
    ScrollView,

    TouchableOpacity,
    ImageBackground
} from 'react-native';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchBar from '../components/SearchBar'
import { color } from 'react-native-reanimated';
const width = Dimensions.get('window').width / 2.8 - 35;
// const widths = Dimensions.get('window').widths / 2 - 25;
import { useNavigation } from '@react-navigation/native';

function UserScreen() {
    const navigation = useNavigation();
    return (

        <View style={{ backgroundColor: '#EAEFD9', paddingVertical: 16, flex: 1 }}>

            {/* header  */}
            <View style={{}}>

                <View style={styles.headers}>
                    <View >
                        <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white"
                            onPress={() => navigation.goBack()}

                        />
                    </View>
                    {/* <View style={styles.headerBtn}>
                        <Icon1 name="shoppingcart" size={30} color={COLORS.dark} />
                    </View> */}
                </View>

                {/* header end */}

                {/* profile image section */}
                <View style={styles.virtualTag}>

                    <Image style={{ width: 150, height: 150, borderRadius: 100, borderColor: "white", marginHorizontal: 10, marginVertical: 10 }} source={require('../../assets/profile.jpeg')} />
                </View>

                {/* <View style={styles.backgroundImageContainer}>


                <View style={styles.backgroundImage}>
    
                    <View style={styles.name}>
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: 'white', fontSize: 18 }}>Amit patel</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', alignSelf: 'center', }}>amitpatel005577@gmail.com</Text>
                    </View>

                </View>


            </View> */}


            </View>

            {/* end of profile images */}
            <View style={{ flex: 1, marginTop: 20 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginHorizontal: 10 }}>
                    <TouchableOpacity onPress={() => navigation.navigate("about")} >
                        <View style={styles.cardContainer}>
                            <View style={styles.iconbackgroundndBtn}>
                                <Icon style={styles.iconBtn} name="account-outline" size={25} color={COLORS.dark} />
                            </View>
                            <Text style={styles.textBtn}>About Me</Text>

                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigation.navigate("myorder")}>
                        <View style={styles.cardContainer}>
                            <View style={styles.iconbackgroundndBtn}>
                                <Icon style={styles.iconBtn} name="shopping-outline" size={25} color={COLORS.dark} />
                            </View>

                            <Text style={styles.textBtn} >My Order</Text>

                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigation.navigate("Favourite")}>
                        <View style={styles.cardContainer}>
                            <View style={styles.iconbackgroundndBtn}>
                                <Icon style={styles.iconBtn} name="heart-outline" size={25} color={COLORS.dark} />
                            </View>
                            <Text style={styles.textBtn}>Favourite</Text>

                        </View>
                    </TouchableOpacity>

                </View>


                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginHorizontal: 10 }}>
                   
                    <View style={styles.cardContainerTwo}>
            <View style={styles.iconbackgroundndBtn}>
            <Icon style={styles.iconBtn} name="bell-outline" size={30} color={COLORS.dark} />
            </View>      

             <Text style={styles.textBtn} >Notification</Text>

            </View>

                    <TouchableOpacity onPress={() => navigation.navigate("Setting")} >
                        <View style={styles.cardContainerTwo}>
                            <View style={styles.iconbackgroundndBtn}>
                                <Icon style={styles.iconBtn} name="cog-outline" size={25} color={COLORS.dark} />
                            </View>

                            <Text style={styles.textBtn} >Setting</Text>

                        </View>
                    </TouchableOpacity>
                </View> */}




                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly',marginTop:20}}>
                    <TouchableOpacity onPress={() => navigation.navigate("Support")}>
                        <View style={styles.BottomcardContainer}>
                            <View style={styles.BottomOneiconbackgroundndBtn}>
                                <Icon style={styles.support} name="face-agent" size={25} color={COLORS.dark} />
                            </View>
                            <Text style={styles.textOneBtn}>Support Center</Text>

                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigation.navigate("Verify Phone Number")}>
                        <View style={styles.BottomcardTwoContainer}>
                            <View style={styles.BottomiconbackgroundndBtn}>
                                <Icon style={styles.exit} name="logout" size={25} color={COLORS.dark} />

                            </View>
                            <Text style={styles.textTwoBtn}>Sign Out</Text>

                        </View>
                    </TouchableOpacity>
                </View>
            </View>

        </View>

    )
}
export default UserScreen;

const styles = StyleSheet.create({
    backgroundImageContainer: {
        alignItems: 'center',
        height: 200,
        marginHorizontal: 10,
        borderRadius: 20,
        // flex:3
    },
    virtualTag: {
        // top: -250,

        borderRadius: 100,


        alignSelf: 'center'
    },

    header: {

    },

    headers: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
    },

    headerBtn: {
        height: 50,
        width: 50,
        backgroundColor: COLORS.white,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },


    backgroundImage: {
        height: '80%',
        width: '100%',
        overflow: 'hidden',
        top: 40,
        borderRadius: 10,
        backgroundColor: '#8AC02A'

    },
    name: {
        marginHorizontal: 80,
        marginVertical: 85
    }
    ,
    cardContainer: {
        // flex:2,
        height: 100,
        backgroundColor: '#F8F8F8',
        width,
        marginHorizontal: 2,
        borderRadius: 24,
        // top:120,
        // marginTop: 120,
        // marginBottom: 20,
        padding: 15,

    },
    cardContainerTwo: {
        height: 100,
        backgroundColor: '#F8F8F8',
        width,
        marginHorizontal: 2,
        borderRadius: 24,
        marginTop: 20,
        // marginBottom: 20,
        padding: 15,

    },
    iconBtn: {
        alignSelf: 'center',
        color: 'orange',
        backgroundColor: 'white'

    },
    textBtn: {
        alignSelf: 'center',
        // marginTop: -5,
        fontWeight: "bold",
        fontSize: 11,
        color: "black"

    },
    iconbackgroundndBtn: {
        height: 60,
        width: 60,
        backgroundColor: COLORS.white,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'

    },
    BottomcardContainer: {
        height: 100,
        backgroundColor: '#F8F0DB',
        width,
        marginHorizontal: 2,
        borderRadius: 24,
        // marginTop: 20,
        // marginBottom: 20,
        padding: 8,

    },
    support: {
        alignSelf: 'center',
        color: 'orange',
        backgroundColor: 'white'

    },
    BottomcardTwoContainer: {
        height: 100,
        backgroundColor: '#EFF7E0',
        width,

        borderRadius: 24,
        // marginTop: 20,
        // marginBottom: 20,
        padding: 8,
        // right:70

    },
    BottomiconbackgroundndBtn: {
        height: 60,
        width: 60,
        backgroundColor: '#89BF29',
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'

    },
    BottomOneiconbackgroundndBtn: {
        height: 60,
        width: 60,
        backgroundColor: "orange",
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'

    },
    textOneBtn: {
        alignSelf: 'center',
        marginTop: 5,
        fontWeight: "bold",
        fontSize: 11,
        color: "orange",
        // fontSize: 14,

    },
    textTwoBtn: {
        alignSelf: 'center',
        marginTop: 5,
        fontWeight: "bold",
        // fontSize: 15,
        color: "#89BF29",
        fontSize: 11

    },
    exit: {
        alignSelf: 'center',
        color: '#89BF29',
        backgroundColor: 'white'

    }


})