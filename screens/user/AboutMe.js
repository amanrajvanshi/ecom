import React from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,

    TouchableOpacity
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

function AboutMe({ navigation }) {
    return (
        <SafeAreaView
            style={{ flex: 1, paddingHorizontal: 20, backgroundColor: COLORS.white }}>



            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.navigate('profile')}>



                    <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white" />

                </TouchableOpacity>
                <View style={styles.searchBar}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold',marginLeft:"38%" }}>About Me</Text>

                </View>
                <TouchableOpacity onPress={() => navigation.navigate('newadd')}>
                    <View style={styles.cart}>

                        <FontAwesome name="edit" size={24} color="orange" />


                    </View>
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{ fontSize: 23, fontWeight: 'bold' }}>
                    Personal Details
                </Text>
            </View>
            <View style={{ paddingHorizontal: 20, flexDirection: 'row', backgroundColor: COLORS.light, height: 70, marginTop: 10, borderRadius: 10 }}>
                <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29, marginTop: 19 }}>
                    <Icon name="account" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                </View>
                <Text
                    style={{
                        color: 'black',
                        fontSize: 15,
                        lineHeight: 22,
                        marginTop: 19,
                        fontWeight: 'bold',
                        marginLeft: 15
                    }}>
                    Amit Patel

                            </Text>
            </View>

            <View style={{ paddingHorizontal: 20, flexDirection: 'row', backgroundColor: COLORS.light, height: 70, marginTop: 10, borderRadius: 10 }}>
                <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29, marginTop: 19 }}>
                    <Icon name="email" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                </View>
                <Text
                    style={{
                        color: 'black',
                        fontSize: 15,
                        lineHeight: 22,
                        marginTop: 19,
                        fontWeight: 'bold',
                        marginLeft: 15
                    }}>
                    amitpatel005577@gmail.com

                            </Text>




            </View>

            <View style={{ paddingHorizontal: 20, flexDirection: 'row', backgroundColor: COLORS.light, height: 70, marginTop: 10, borderRadius: 10 }}>
                <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29, marginTop: 19 }}>
                    <Icon name="phone" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                </View>
                <Text
                    style={{
                        color: 'black',
                        fontSize: 15,
                        lineHeight: 22,
                        marginTop: 19,
                        fontWeight: 'bold',
                        marginLeft: 15
                    }}>
                    +22213321612

                            </Text>




            </View>







        </SafeAreaView>

    )
}
export default AboutMe;



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        // marginTop: 5,
        marginHorizontal: -13,
        paddingBottom: 17,




    },



    searchBar: {
        flexDirection: 'row',
        flex: 1,

        // paddingHorizontal: 115,
        paddingVertical: 10,


    },

    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },






});