import * as React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
import { Searchbar } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const SearchScreen = () => {
    const navigation = useNavigation();
    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);

    return (
        
        <View style={{ flex: 1 }}>
               <View style={styles.container}>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>


            <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

          </TouchableOpacity>
          <View style={styles.searchBar}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 45 }}>Search</Text>

          </View>
      
        </View>

         

            <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                <Searchbar
                    placeholder="Search"
                    onChangeText={onChangeSearch}
                    value={searchQuery}
                />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 25 }}>
                <TouchableOpacity>
                    <Text style={{ marginLeft: 20, fontSize: 16, fontWeight: 'bold' }}>Search History</Text>
                </TouchableOpacity>

                <TouchableOpacity>
                    <Text style={{ marginRight: 20 }}>Clear</Text>
                </TouchableOpacity>



            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}> Fresh apple </Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Fresh apricots</Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Biskut cakes </Text></TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}> Fresh apple </Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Bingo m</Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Biskut cakes </Text></TouchableOpacity>

            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 25 }}>
                <TouchableOpacity>
                    <Text style={{ marginLeft: 20, fontSize: 16, fontWeight: 'bold' }}>Discover Mode</Text>
                </TouchableOpacity>

                <TouchableOpacity>
                    <Text style={{ marginRight: 20 }}>Refresh</Text>
                </TouchableOpacity>



            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}> Fresh apple </Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Fresh apricots</Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Biskut cakes </Text></TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}> Fresh apple </Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Bingo m</Text></TouchableOpacity>
                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: 'gray', backgroundColor: 'white', padding: 8, borderRadius: 15 }}>Biskut cakes </Text></TouchableOpacity>

            </View>
        </View>



    );
};

export default SearchScreen;

const styles = StyleSheet.create({



    List: {
      padding: 15
    },
    item: {
      flexDirection: 'row',
      marginTop: 8,
      padding: 8,
      shadowColor: "#000",
      textShadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      backgroundColor: 'white',
      elevation: 3,
      borderRadius: 10
    },
  
    img: {
      height: 70,
      width: 70
    },
    button: {
      justifyContent: 'center', alignItems: 'center', marginTop: 30
      , padding: 12
    },
  
    ImageText: {
      position: 'absolute',
      color: 'white',
      marginTop: 4,
      fontSize: 14,
      left: 5,
      bottom: 10,
      padding: 5,
      backgroundColor: 'black',
      // borderWidth:2,
      borderRadius: 5,
  
  
  
    },
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 5,
      marginLeft: 13,
      marginRight: 13
  
    },
  
    searchBar: {
      flexDirection: 'row',
      flex: 1,
      // borderWidth: 1,
      // borderColor: '#EFEFEF',
      // borderRadius: 8,
      paddingHorizontal: 60,
      paddingVertical: 10,
      fontSize: 50,
      alignItems: 'center',
      marginHorizontal: 12,
      textAlign: 'center',
      alignSelf: 'center'
    },
    input: {
      fontSize: 18,
      marginLeft: 10,
      paddingVertical: 5,
    },
    badge: {
      position: 'absolute',
      backgroundColor: '#E6848C',
      width: 18,
      height: 18,
      borderRadius: 15,
      justifyContent: 'center',
      alignItems: 'center',
      right: -4,
      top: -4,
    },
    badgeText: {
      color: '#fff',
    },
    cart: {
      marginRight: 10
    },
    buttonStyle: {
      backgroundColor: '#8ebc0e',
  
      marginTop: 10,
      borderRadius: 10,
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      width: 365,
      height: 50,
      alignSelf: 'center',
    
  
  
    },
  })