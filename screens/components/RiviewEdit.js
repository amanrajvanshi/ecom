import React, { component } from 'react'
import { View, Text, StyleSheet, SafeAreaView, FlatList, Image, Alert, TouchableOpacity, ImageBackground, ScrollView, TextInput } from 'react-native';

import StarRating from 'react-native-star-rating';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';

import { Searchbar } from 'react-native-paper'
import { color } from 'react-native-reanimated';


export default class RiviewEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 3.5
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }


    render() {

        return (
            <View style={{flex:1}}>
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Riview')}>


                        <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                    </TouchableOpacity>
                    <View style={styles.searchBar}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 40, }}>Add Review</Text>

                    </View>
                 
                </View>
                <ScrollView>
                    <View style={{ borderWidth: 1, borderColor: '#8ebc0e', borderRadius: 8, height: 280, width: 370, backgroundColor: '#8ebc0e', alignSelf: 'center',}}>
                        <Text style={{ marginTop: 20, alignSelf: 'center', fontSize: 26, color: 'white' }}>Rate your experience for</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 15, marginLeft: 70, marginRight: 70 }}>
                            <CircleCheckBox
                                checked={true}
                                innerSize={14}
                                outerSize={20}

                            />
                            <Text style={{ fontSize: 18, color: 'white', }}>Shopping</Text>
                            <CircleCheckBox
                                checked={false}
                                innerSize={12}
                                outerSize={20}

                            />
                            <Text style={{ fontSize: 18, color: 'white', }}>Delivery</Text>
                        </View>
                        <View style={{ alignSelf: 'flex-end', width: 240, marginRight: 30, marginTop: 20 }}>
                            <View>

                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={this.state.starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'orange'}
                                    starSize={20}





                                />
                            </View>
                        </View>

                        <View style={{ alignSelf: 'flex-end', width: 240, marginRight: 30, marginTop: 10 }}>
                            <View>

                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={this.state.starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'orange'}
                                    starSize={20}





                                />
                            </View>
                        </View>

                        <View style={{ alignSelf: 'flex-end', width: 240, marginRight: 30, marginTop: 10 }}>
                            <View>

                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={this.state.starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'orange'}
                                    starSize={20}





                                />
                            </View>
                        </View>
                        <View style={{ alignSelf: 'flex-end', width: 240, marginRight: 30, marginTop: 10 }}>
                            <View>

                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={this.state.starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'orange'}
                                    starSize={20}





                                />
                            </View>
                        </View>

                        <View style={{ alignSelf: 'flex-end', width: 240, marginRight: 30, marginTop: 10 }}>
                            <View>

                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={this.state.starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'orange'}
                                    starSize={20}





                                />
                            </View>
                        </View>



                    </View>

                    <View style={{ borderWidth: 1, backgroundColor: 'white', borderColor: '#DCDCDC', borderTopLeftRadius: 8, borderTopRightRadius: 8, height: 60, width: 380, marginTop: 25, alignSelf: 'center' }}>
                        <Text style={{ fontSize: 21, fontWeight: 'bold', marginTop: 10, alignSelf: 'flex-start', marginLeft: 15 }}>What was not up to the mark?</Text>

                    </View>
                    <View style={{ borderWidth: 1, borderBottomLeftRadius: 8, borderBottomRightRadius: 8, borderColor: '#DCDCDC', height: 240, width: 380, alignSelf: 'center' }}>
                        <View style={{ marginTop: 15, width: 340, alignSelf: 'center', height: 20, marginBottom: 20 }}>
                            <Searchbar
                                placeholder="Search tag or search from below"


                            />
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 30, marginRight: 20 }}>
                            <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}> Fresh apple </Text></TouchableOpacity>
                            <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', color: 'white', backgroundColor: 'orange', padding: 4, borderRadius: 5, height: 30 }}>Fresh apricots</Text></TouchableOpacity>
                            <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}>Biskut cakes </Text></TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20, marginRight: 60 }}>
                            <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}> Fresh apple </Text></TouchableOpacity>
                            <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}>Bingo m</Text></TouchableOpacity>
                            <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}>Biskut cakes </Text></TouchableOpacity>

                        </View>
                    </View>

                    <View>
                        <View style={{ borderWidth: 1, backgroundColor: 'white', borderColor: '#DCDCDC', borderTopLeftRadius: 8, borderTopRightRadius: 8, height: 60, width: 380, marginTop: 25, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 21, fontWeight: 'bold', marginTop: 10, alignSelf: 'flex-start', marginLeft: 15 }}>What did you like?</Text>

                        </View>
                        <View style={{ borderWidth: 1, borderBottomLeftRadius: 8, borderBottomRightRadius: 8, borderColor: '#DCDCDC', height: 240, width: 380, alignSelf: 'center' }}>
                            <View style={{ marginTop: 15, width: 340, alignSelf: 'center', height: 20, marginBottom: 30 }}>
                                <Searchbar
                                    placeholder="Search tag or search from below"


                                />
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20, marginRight: 20 }}>
                                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}> Fresh apple </Text></TouchableOpacity>
                                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'orange', color: 'white', padding: 4, borderRadius: 5, height: 30 }}>Fresh apricots</Text></TouchableOpacity>
                                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}>Biskut cakes </Text></TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20, marginRight: 60 }}>
                                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}> Fresh apple </Text></TouchableOpacity>
                                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}>Bingo m</Text></TouchableOpacity>
                                <TouchableOpacity><Text style={{ borderWidth: 1, borderColor: '#D0D0D0', backgroundColor: 'white', padding: 4, borderRadius: 5, height: 30 }}>Biskut cakes </Text></TouchableOpacity>

                            </View>
                        </View>

                    </View>

                    <View>
                        <View style={{ borderWidth: 1, backgroundColor: 'white', borderColor: '#DCDCDC', borderTopLeftRadius: 8, borderTopRightRadius: 8, height: 60, width: 380, marginTop: 25, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 21, fontWeight: 'bold', marginTop: 10, alignSelf: 'flex-start', marginLeft: 15 }}>Add Photos</Text>

                        </View>
                        <View style={{ borderWidth: 1, borderBottomLeftRadius: 8, borderBottomRightRadius: 8, borderColor: '#DCDCDC', height: 160, width: 380, alignSelf: 'center' }}>

                            <TouchableOpacity>
                                <View style={{ borderWidth: 1, borderColor: '#DCDCDC', height: 100, width: 100, alignSelf: 'flex-start', marginTop: 20, marginLeft: 20, backgroundColor: '#D0D0D0' }}>
                              
                              <View style = {{marginTop: 20, alignSelf: 'center'}}>
                              <Ionicons name="camera-outline" size={45} color="white" backgroundColor="white" />
                              </View>
                                </View>
                            </TouchableOpacity>

                        </View>

                    </View>

                    <View>
                        <View style={{ borderWidth: 1, backgroundColor: 'white', borderColor: '#DCDCDC', borderTopLeftRadius: 8, borderTopRightRadius: 8, height: 60, width: 380, marginTop: 25, alignSelf: 'center' }}>
                            <Text style={{ fontSize: 21, fontWeight: 'bold', marginTop: 10, alignSelf: 'flex-start', marginLeft: 15 }}>Write a review</Text>

                        </View>
                        <View style={{ borderWidth: 1, borderBottomLeftRadius: 8, borderBottomRightRadius: 8, borderColor: '#DCDCDC', height: 140, width: 380, alignSelf: 'center' }}>

                            <View style={{ borderWidth: 1, borderRadius: 8, borderColor: '#DCDCDC', height: 100, width: 350, alignSelf: 'center', marginTop: 20, backgroundColor: 'white' }}>
                                <TextInput
                                    placeholder="Your review will help people"
                                    style={{ marginLeft: 15, fontSize: 17, marginTop: 10 }}
                                />
                            </View>
                        </View>

                    </View>



                </ScrollView>
               <View style={{}}>
                    <TouchableOpacity style={styles.buttonStyle}
                        onPress={() => this.props.navigation.navigate('Riview')}
                    >
                        <Text style={{ color: 'white', fontWeight: 'bold' }}>Submit Shopping Review</Text>
                    </TouchableOpacity>
               
                    </View>
            </View>








        )
    }
}


const styles = StyleSheet.create({



    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 13,
        marginRight: 13

    },

    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        backgroundColor: '#8ebc0e',

     
        borderRadius: 10,
        // position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 365,
        height: 50,
        alignSelf: 'center',
        marginBottom:"2%"
        // marginTop: "125%"


    },
})
