import React, { useState } from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,

  TouchableOpacity,
  Linking,
  Modal,
  ImageBackground,
  ScrollView,

  Pressable
} from 'react-native';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import SubCategory from '../../assets/Data/subCategory'
// import RadioGroup,{RadioButton} from 'react-native-radio-buttons-group';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
// import DropDownPicker from 'react-native-dropdown-picker';
// import PickerCheckBox from 'react-native-picker-checkbox';
// import RadioButtonsGroup from 'react-native-radio-buttons-group';
import { RadioButton } from 'react-native-paper';
const width = Dimensions.get('window').width / 2 - 15;





// function tapToLike(likeIcon) {
//   if (likeIcon % 2 === 0) {
//     return images.redHeart;
//   } else {
//     return images.like;
//   }
// }





function Fruit({ navigation }) {
  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);
  const [selectedLanguage, setSelectedLanguage] = useState();


  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [modalVisible, setModalVisible] = useState(false)

  const [checked, setChecked] = React.useState('first');
  // const [likeIcon, setLikeIcon] = React.useState(1);

 
//like 












  // categories component


  const renderItem = ({ item }) => {
    return (

      <View style={styles.categoryContainer}>
        <TouchableOpacity
        //  onPress={()=>onSelectCategory(item)}
        >
          <View style={{ borderRadius: 15, height: 34, width: 90, justifyContent: 'center', borderWidth: 1, borderColor: 'red', }}>
            <Text style={{ alignSelf: 'center', color: 'orange', fontWeight: 'bold',padding:8 }}

            >{item.name}</Text>
          </View>

        </TouchableOpacity>

      </View>

    );
  };







  // card component 


  const Card = ({ plant }) => {

    return (
      <TouchableOpacity
        // activeOpacity={0.8}
        onPress={() => navigation.navigate('Item', plant)}>
        <View style={styles.card}>
          <View style={{ flexDirection:'row',justifyContent:'space-between' }}>
            <View style={{backgroundColor:"#59A725",justifyContent:'center',borderRadius:5,width:"40%"}}>
              <Text style={{color:'white',alignSelf:'center'}}>5% OFF</Text>
            </View>



            
            <View
              style={{
                width: 30,
                height: 30,
                borderRadius: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: plant.like
                  ? 'rgba(245, 42, 42,0.2)'
                  : 'rgba(0,0,0,0.2) ',
              }}>
              <Icon
                name="heart"
                size={18}
                color={plant.like ? COLORS.red : COLORS.black}
              />
            </View>
          </View>

          <View
            style={{
              height: 100,
              alignItems: 'center',
            }}>
            <Image
              source={plant.img}
              style={{ flex: 1, resizeMode: 'contain' }}
            />
          </View>

          <Text style={{  fontSize: 17, marginTop: 5 }}>
            {plant.name}
          </Text>
          <Text style={{  fontSize: 17, marginTop: 5 }}>
            {plant.weight}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 5,
            }}>
            <Text style={{ fontSize: 19, fontWeight: 'bold' }}>
              ${plant.price}
            </Text>
       
          </View>
          <TouchableOpacity>
          <View style={{flexDirection:'row',justifyContent:"space-between",backgroundColor:'#E86225',width:'100%',padding:5,marginTop:4,borderRadius:4}}>
            <Text style={{color:'white',marginLeft:'30%'}}>Add</Text>
            <View style={{backgroundColor:"#C0521D",width:'20%',alignSelf:'flex-end'}}>
            <Text style={{color:'white',alignSelf:'center'}}>+</Text>
            </View>
          </View>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };





  return (
    <SafeAreaView
      style={{ flex: 1, backgroundColor: COLORS.light, height: 60, }}>
      <View style={{
        backgroundColor: 'black',
        paddingHorizontal: 10
        // 
      }}>
        <View style={{
          // flex:1,
          flexDirection: "row",
justifyContent:'space-between',
          // width: "100%",
          marginTop: 10,

        }}>


          <View style={{}}>

            <Ionicons name='chevron-back-outline' size={30}
              backgroundColor="#F4F5F9" color='white' onPress={() => navigation.goBack()} />
          </View>

          <View style={{ flexDirection: 'column',  paddingBottom: "2%",marginLeft:"-19%" }}>
            <Text style={{ color: 'lightgrey' }}>Your Location</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ fontSize: 16, color: 'white' }}>Cannaught place,New Delhi</Text>
              <Ionicons name='chevron-down-outline' color={"white"} size={20} />
            </View>
          </View>
          <View style={{  }}>
          {/* <Icon1 name="shoppingcart" size={30} color="gray" /> */}
            <Icon1 name='shoppingcart' size={30}
              backgroundColor="#F4F5F9" color='white' />
          </View>

        </View>

      </View>
      <View style={{flexDirection:'row',paddingHorizontal:18}}>
        <Text style={{fontSize:14,color:'grey'}}>Home </Text>
        <Ionicons name='chevron-forward-outline' size={20}
              backgroundColor="#F4F5F9" color='grey' />
        <Text style={{color:'grey'}}>Grocery & staples</Text>
        <Ionicons name='chevron-forward-outline' size={20}
              backgroundColor="#F4F5F9" color='grey' />
        <Text>pulses</Text>

      </View>




      <View style={{marginTop: 10, }}>

        <View>
          <Pressable
            onPress={() => setModalVisible(true)}
          >

            <View style={{flexDirection:'row',justifyContent:"space-evenly"}}> 
            <View style={{ flexDirection: 'row', justifyContent: "flex-start" }}>
              <Text style={{color:"grey",justifyContent:"center",alignSelf:'center',fontSize:16}}>Sort by</Text>

            </View>
            <View style={{width:'70%',backgroundColor:'white',justifyContent:'center',borderColor:'black',borderWidth:1,padding:7}}>
            <Text style={{paddingLeft:'2%',fontWeight:'bold',}}>
              Max to min
            </Text>
            </View>
            </View>
          </Pressable>
        </View>

        <View>
          <Modal

            animationType="slide"
            transparent={true}
            visible={modalVisible}
          >
            <View style={styles.modalContainer}>
              <View style={styles.modalView}>
                <View>
                  <View style={{ flexDirection: 'row' }}>
                    <RadioButton
                      value="first"
                      status={checked === 'first' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('first')}
                      color="orange"
                    />
                    <View style={{ justifyContent: 'center' }}>
                      <Text style={{ justifyContent: "center" }}>Max to Min</Text>
                    </View>
                  </View>
{/* second btn  */}

                  <View style={{ flexDirection: 'row' }}>
                    <RadioButton
                      value="second"
                      status={checked === 'second' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('second')}
                      color="orange"
                    />
                    <View style={{ justifyContent: 'center' }}>
                      <Text style={{ justifyContent: "center" }}>Low to High</Text>
                    </View>
                  </View>

 {/* third btn                   */}
                  <View style={{ flexDirection: 'row' }}>
                    <RadioButton
                      value="third"
                      status={checked === 'third' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('third')}
                      color="orange"
                    />
                    <View style={{ justifyContent: 'center' }}>
                      <Text style={{ justifyContent: "center" }}>Premium</Text>
                    </View>
                  </View>

{/* forth btn  */}

                  <View style={{ flexDirection: 'row' }}>
                    <RadioButton
                      value="fourth"
                      status={checked === 'fourth' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('fourth')}
                      color="orange"
                    />
                    <View style={{ justifyContent: 'center' }}>
                      <Text style={{ justifyContent: "center" }}>High Quality</Text>
                    </View>
                  </View>
{/* fifth btn  */}
                  <View style={{ flexDirection: 'row' }}>
                    <RadioButton
                      value="fifth"
                      status={checked === 'fifth' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('fifth')}
                      color="orange"
                    />
                    <View style={{ justifyContent: 'center' }}>
                      <Text style={{ justifyContent: "center" }}>Low Quality</Text>
                    </View>
                  </View>
                </View>


<View style={{justifyContent:'center',marginTop:'20%'}}>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => setModalVisible(!modalVisible)}

                >
                  <Text style={styles.textStyle}>Apply</Text>
                </Pressable>
                </View>
              </View>



            </View>

          </Modal>
        </View>








      </View>
      {/* <CategoryList /> */}
      <View style={{ paddingLeft: 5 }}>

        <FlatList
          horizontal={true}
          data={SubCategory}
          showsHorizontalScrollIndicator={false}
          keyExtractor={item => `${item.id}`}
          renderItem={renderItem}
        // contentContainerStyle={{paddingVertical:SI}}


        />
      </View>

      <SafeAreaView
        style={{ flex: 1, paddingHorizontal: 10, backgroundColor: COLORS.light, top: 10, }}>


        <FlatList
          columnWrapperStyle={{ justifyContent: 'space-between' }}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            // marginTop: 40,
            // paddingBottom: 20,
          }}
          numColumns={2}
          data={CardData}
          renderItem={({ item }) => {
            return <Card plant={item} />;
          }}
        />


      </SafeAreaView>
    </SafeAreaView>



  )
}
export default Fruit;


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    // backgroundColor:'red',
    height: 150

  },
  backgroundImageContainer: {
    elevation: 20,
    alignItems: 'center',
    height: 200,
    // paddingBottom:20
  },
  virtualTag: {
    top: -80,
    width: 120,
    borderRadius: 100,
    height: 120,
    paddingHorizontal: 20,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },



  header: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  searchContainer: {
    height: 50,
    backgroundColor: COLORS.light,
    borderRadius: 10,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },


  ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 5,
    bottom: 10,
    padding: 5,
    backgroundColor: 'black',
    // borderWidth:2,
    borderRadius: 5,



  },

  backgroundImage: {
    height: '100%',
    width: '100%',
    // borderRadius: 20,
    overflow: 'hidden',
    borderBottomEndRadius: 20,
    borderBottomLeftRadius: 20
  },


  searchBar: {
    flexDirection: 'row',
    flex: 1,
    // borderWidth: 1,
    // borderColor: '#EFEFEF',
    // borderRadius: 8,
    paddingHorizontal: 85,
    paddingVertical: 10,
    fontSize: 50,
    alignItems: 'center',
    marginHorizontal: 12,
    textAlign: 'center',
    alignSelf: 'center'
  },
  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },
  badge: {
    position: 'absolute',
    backgroundColor: '#E6848C',
    width: 18,
    height: 18,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    right: -4,
    top: -4,
  },
  badgeText: {
    color: '#fff',
  },
  cart: {
    // marginRight: 10,
    marginTop: 5,
    alignSelf: 'center',
  },


  card: {
    height: 270,
    backgroundColor: COLORS.white,
    width,
    // marginHorizontal: 2,
    borderRadius: 10,
    marginBottom: 20,
    padding: 10,
  },
  header: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headers: {
    // paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  headerBtn: {
    height: 50,
    width: 50,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryContainer: {


    marginTop: 20,
    padding: 5

    // marginHorizontal:10,
    // justifyContent: 'space-evenly',

  },
  categoryText: {
    fontSize: 16, color: 'grey', fontWeight: 'bold', paddingBottom: 5,
    borderBottomWidth: 2,
    borderColor: COLORS.green,
  },
  // onselectCategory: {
  //   color: COLORS.green,
  //   paddingBottom: 5,
  //   borderBottomWidth: 2,
  //   borderColor: COLORS.green,
  // },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    flex: .5,
    // height: '50%',
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 5,
    margin: 20,
    padding: 20,
    justifyContent: 'flex-start'
    // alignContent: 'center'
  },
  button: {
    borderRadius: 10,
// color:'white',
    // padding: 5,
    elevation: 2,
    width: '100%'
  },
  buttonOpen: {
    backgroundColor: "white",
    width:"25%",
    borderRadius:5
  },
  buttonClose: {
    backgroundColor: COLORS.orange,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center",
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0, .6)'
  }


});