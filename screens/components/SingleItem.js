import React from 'react';
import { View, SafeAreaView, Image, Text, StyleSheet, ScrollView, Dimensions, TouchableOpacity } from 'react-native';
import { color } from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../assets/Color';
import Category from '../home/Category';
import FreshPickup from '../home/FreshPickup';
import StarRating from 'react-native-star-rating';
import NumericInput from 'react-native-numeric-input'
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const width = Dimensions.get('window').width / 1.2 - 30;




//  plant = product and props which is given every where 



const SingleItem = ({ navigation, route }) => {
    const plant = route.params;

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: '#F3F4F8',
            }}>

            <View style={style.header}>
                <Ionicons name="chevron-back-outline" size={28} onPress={() => navigation.goBack()} />
                {/* <View style={{  backgroundColor: 'orange', width: 40, height: 40, borderRadius: 150 / 2,alignSelf:'center',}}>
        <View style={{alignSelf:'center',marginTop:5}}>
         
          <Icon1 name="shoppingcart" size={30} color="white"  />
      
        </View>
        </View> */}
            </View>
            <ScrollView style={{ marginBottom: 30 }}>
                <View style={style.imageBackground}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                        <View style={{
                            backgroundColor: '#EEFACA',
                            width: 120,
                            height: 40,
                            // marginLeft:'30%',
                            justifyContent: 'center',
                            borderTopRightRadius:8,
                            borderBottomRightRadius: 8,
                            // borderTopLeftRadius: 8,
                            // borderBottomLeftRadius: 8,
                            left: -20
                        }}>
                            <Text style={{ color: '#8CC970', fontWeight: 'bold',alignSelf:'center' }}>in Stock</Text>
                        </View>
                        <View
                            style={{
                                width: 30,
                                height: 30,
                                borderRadius: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: plant.like
                                    ? 'rgba(245, 42, 42,0.2)'
                                    : 'rgba(0,0,0,0.2) ',
                            }}>
                            <Icon
                                name="favorite-border"
                                size={18}
                                color={plant.like ? COLORS.red : COLORS.black}
                            />
                        </View>
                    </View>

                    <View style={style.imageContainer}>
                        <Image source={plant.img} style={{ resizeMode: 'contain', flex: 1, width: "120%", top: -40 }} />
                    </View>

                    <View style={style.imageContent}>
                        <Text style={{ fontWeight: 'bold', fontSize: 13, }}>
                            {plant.tag}
                        </Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 30, }}>
                            {plant.name}
                        </Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 19, }}>
                            {plant.weight}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                                marginTop: 5,
                            }}>
                            <Text style={{ fontSize: 19, fontWeight: 'bold' }}>
                                Rs {plant.price}
                            </Text>
                            {/* <View
                                style={{
                                    height: 25,
                                    width: 25,
                                    backgroundColor: COLORS.orange,
                                    borderRadius: 5,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Icon
                                    name="shopping-cart"
                                    size={18}
                                    color={plant.like ? COLORS.white : COLORS.white}
                                />
                            </View> */}
                        </View>
                    </View>


                    {/* increese and decreese function require here  */}

                    <View style={style.increaseBackground}>

                        <View style={{
                            marginTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                            <View style={style.Quantity}>
                                <Text
                                    style={{ color: COLORS.dark, fontSize: 18, fontWeight: 'bold' }}>
                                    Quantity
                                 </Text>
                            </View>



                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    left: -30,
                                    top: -8
                                }}>
                                <NumericInput
                                    totalWidth={100}
                                    totalHeight={30}

                                    step={1}
                                />


                            </View>


                        </View>
                    </View>




                    {/* detail description of product */}

                    <View style={style.detailsContainer}>
                        <View
                            style={{
                                marginLeft: 20,
                                // marginTop: 20,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                            <Text style={{ fontSize: 22, fontWeight: 'bold' }}>Description</Text>

                        </View>
                        <View style={{ paddingHorizontal: 20, marginTop: 10 }}>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontSize: 16,
                                    lineHeight: 22,
                                    marginTop: 10,
                                }}>
                                {plant.about}
                            </Text>

                        </View>
                    </View>


                    {/* product information  */}



                    <View style={style.detailsContainer}>
                        <View
                            style={{
                                marginLeft: 20,
                                // marginTop: 20,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                            <Text style={{ fontSize: 22, fontWeight: 'bold' }}>Product Information</Text>

                        </View>

                        <View style={{ paddingRight: 20, paddingLeft: 20 }}>
                            <View style={{ paddingHorizontal: -5, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1 }}>
                                <Text
                                    style={{
                                        color: '#79746E',
                                        fontSize: 16,
                                        lineHeight: 22,
                                        fontWeight: 'bold',
                                        marginTop: 10,
                                    }}>
                                    Brand
                        </Text>
                                <Text
                                    style={{
                                        color: 'grey',
                                        fontSize: 15,
                                        lineHeight: 22,
                                        marginTop: 10,
                                        fontWeight: 'bold'
                                    }}>
                                    {plant.brand}

                                </Text>




                            </View>
                        </View>
                        <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                            <View style={{ paddingHorizontal: -5, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1, }}>
                                <Text
                                    style={{
                                        color: '#79746E',
                                        fontSize: 16,
                                        lineHeight: 22,
                                        fontWeight: 'bold',
                                        marginTop: 10,
                                    }}>
                                    Manufacturer
                        </Text>
                                <Text
                                    style={{
                                        color: 'grey',
                                        fontSize: 15,
                                        fontWeight: 'bold',
                                        lineHeight: 22,
                                        marginTop: 10,
                                    }}>
                                    {plant.Manufacturer}

                                </Text>

                            </View>
                        </View>

                        <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                            <View style={{ paddingHorizontal: -5, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1 }}>
                                <Text
                                    style={{
                                        color: '#79746E',
                                        fontSize: 16,
                                        lineHeight: 22,
                                        fontWeight: 'bold',
                                        marginTop: 10,
                                    }}>
                                    Country of Origin
                        </Text>
                                <Text
                                    style={{
                                        color: 'grey',
                                        fontSize: 15,
                                        lineHeight: 22,
                                        marginTop: 10,
                                        fontWeight: 'bold'
                                    }}>
                                    {plant.country}

                                </Text>




                            </View>
                        </View>

                        <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', }}>
                            <Text
                                style={{
                                    color: '#79746E',
                                    fontSize: 16,
                                    lineHeight: 22,
                                    fontWeight: 'bold',
                                    marginTop: 10,
                                }}>
                                Food Type
                        </Text>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontSize: 15,
                                    lineHeight: 22,
                                    marginTop: 10,
                                    fontWeight: 'bold'
                                }}>
                                {plant.foodType}

                            </Text>




                        </View>
                    </View>



                    {/* product rating */}

                    <View style={style.detailsContainer}>
                        <View
                            style={{
                                marginLeft: 20,
                                // marginTop: 20,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                            <Text style={{ fontSize: 22, fontWeight: 'bold' }}>Product Rating</Text>

                        </View>
                        <View style={{ paddingHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', }}>
                            <Text
                                style={{
                                    color: 'grey',
                                    fontSize: 16,
                                    lineHeight: 22,
                                    fontWeight: 'bold',
                                    marginTop: 10,

                                }}>
                                Rate Product
                        </Text>
                            <View style={{ marginLeft: 10, marginTop: 10 }}>
                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}


                                    fullStarColor={'orange'}
                                    starSize={20}





                                />
                            </View>
                        </View>

                        <View style={style.Review}>
                            <TouchableOpacity style={style.buttonStyle}
                                onPress={() => navigation.navigate('Riview')}
                            // onPress={() =>{si }  
                            >
                                <Text style={style.text2}>Write Riview</Text>
                            </TouchableOpacity>
                        </View>

                    </View>



                    {/* Add to cart  */}




                </View>




                <Category />

            </ScrollView>

            <View style={style.addCartBtnBackground}>
                <TouchableOpacity style={style.addCartBtn}
                    onPress={() => navigation.navigate('Order')}
                // onPress={() =>{si }  
                >
                    <Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Add to Cart</Text>
                </TouchableOpacity>
            </View>

        </SafeAreaView>
    );
};

const style = StyleSheet.create({
    header: {
        paddingHorizontal: 20,
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        bottom: 10
    },
    imageContent: {
        top: -80


    },
    imageBackground: {
        flex: 1,
        // top:20,
        // marginTop: -20,
        // justifyContent: 'center',
        // alignItems: 'center',
        // flex: 2,
        backgroundColor: COLORS.white,
        marginHorizontal: 16,
        // marginBottom: 70,
        borderRadius: 20,
        // marginTop: 30,
        paddingHorizontal: 20,
    },
    imageContainer: {
        flex: 0.35,

        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: COLORS.white,
        marginHorizontal: 8,
        marginBottom: 7,
        borderRadius: 20,
        marginTop: 40,
        // paddingTop: 30,
        height: 225,
        // backgroundColor: COLORS.white,
        width,
        // marginHorizontal: 2,
        // borderRadius: 10,
        marginBottom: 20,
        padding: 15,
    },
    detailsContainer: {
        // flex: 0.55,
        backgroundColor: COLORS.light,
        // marginHorizontal: 7,
        marginBottom: 7,
        borderRadius: 10,
        top: -30,
        paddingTop: 10,
    },
    increaseBackground: {
        top: -50,
        backgroundColor: COLORS.light,
        borderRadius: 10,

    },
    line: {
        width: 25,
        height: 2,
        backgroundColor: COLORS.dark,
        marginBottom: 5,
        marginRight: 3,
    },
    borderBtn: {
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        width: 60,
        height: 40,
    },
    borderBtnText: { fontWeight: 'bold', fontSize: 28 },
    Quantity: {
        width: 80,
        height: 50,
        backgroundColor: COLORS.light,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        top: -8
    },
    downIcon: {

        justifyContent: 'center',
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
    },
    Review: {
        alignSelf: 'center',
        marginTop: 20,
        width: 80,
        height: 100,
        borderColor: 'grey',
        // borderWidth: 1,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        // width: 60,
        // height: 40,
        // top: 18

    },

    text2: {

        color: 'black',
        fontSize: 16,
        fontWeight: 'bold'
    },


    buttonStyle: {
        backgroundColor: 'white',

        marginTop: 210,
        borderRadius: 10,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 50,
        alignSelf: 'center',
        // backgroundColor: "green",
        borderWidth: 1

    },
    addCartBtnBackground: {
        alignSelf: 'center',
        marginTop: -35,

        width: 80,
        height: 100,
        borderColor: 'grey',
        // borderWidth: 1,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',

    },
    addCartBtn: {
        backgroundColor: 'gray',

        // marginTop: 10,
        borderRadius: 8,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 350,
        height: 50,
        alignSelf: 'center',
        backgroundColor: '#80B026',
        borderWidth: 1

    }
});

export default SingleItem;