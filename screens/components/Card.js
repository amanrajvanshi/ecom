// import React from 'react';
// import {
//   View,
//   SafeAreaView,
//   Text,
//   StyleSheet,
//   FlatList,
//   Image,
//   Dimensions,
// } from 'react-native';
// import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import COLORS from '../../assets/Color';
// // import CardData from '../../CardData';
// const width = Dimensions.get('window').width / 2 - 30;

// // plant is a props   

//   const Card = ({plant}) => {

//     return (
//       <TouchableOpacity
//         // activeOpacity={0.8}
//         onPress={() => navigation.navigate('Home', plant)}>
//         <View style={style.card}>
//           <View style={{alignItems: 'flex-end'}}>
//             <View
//               style={{
//                 width: 30,
//                 height: 30,
//                 borderRadius: 20,
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 backgroundColor: plant.like
//                   ? 'rgba(245, 42, 42,0.2)'
//                   : 'rgba(0,0,0,0.2) ',
//               }}>
//               <Icon
//                 name="heart"
//                 size={18}
//                 color={plant.like ? COLORS.red : COLORS.black}
//               />
//             </View>
//           </View>

//           <View
//             style={{
//               height: 100,
//               alignItems: 'center',
//             }}>
//             <Image
//               source={plant.img}
//               style={{flex: 1, resizeMode: 'contain'}}
//             />
//           </View>

//           <Text style={{fontWeight: 'bold', fontSize: 17, marginTop: 10}}>
//             {plant.name}
//           </Text>
//           <View
//             style={{
//               flexDirection: 'row',
//               justifyContent: 'space-between',
//               marginTop: 5,
//             }}>
//             <Text style={{fontSize: 19, fontWeight: 'bold'}}>
//               ${plant.price}
//             </Text>
//             <View
//               style={{
//                 height: 25,
//                 width: 25,
//                 backgroundColor: COLORS.orange,
//                 borderRadius: 5,
//                 justifyContent: 'center',
//                 alignItems: 'center',
//               }}>
//                <Icon
//                 name="cart-outline"
//                 size={18}
//                 color={plant.like ? COLORS.white : COLORS.white}
//               />
//             </View>
//           </View>
//         </View>
//       </TouchableOpacity>
//     );
//   };


// export default Card;

//   const style = StyleSheet.create({
//     categoryContainer: {
//       flexDirection: 'row',
//       marginTop: 30,
//       marginBottom: 20,
//       justifyContent: 'space-between',
//     },
//     categoryText: {fontSize: 16, color: 'grey', fontWeight: 'bold'},
//     categoryTextSelected: {
//       color: COLORS.green,
//       paddingBottom: 5,
//       borderBottomWidth: 2,
//       borderColor: COLORS.green,
//     },
//     card: {
//       height: 225,
//       backgroundColor: COLORS.white,
//       width,
//       marginHorizontal: 2,
//       borderRadius: 10,
//       marginBottom: 20,
//       padding: 15,
//     },
//     header: {
//       marginTop: 30,
//       flexDirection: 'row',
//       justifyContent: 'space-between',
//     },
//     searchContainer: {
//       height: 50,
//       backgroundColor: COLORS.light,
//       borderRadius: 10,
//       flex: 1,
//       flexDirection: 'row',
//       alignItems: 'center',
//     },
//     input: {
//       fontSize: 18,
//       fontWeight: 'bold',
//       flex: 1,
//       color: COLORS.dark,
//     },
//     sortBtn: {
//       marginLeft: 10,
//       height: 50,
//       width: 50,
//       borderRadius: 10,
//       backgroundColor: COLORS.green,
//       justifyContent: 'center',
//       alignItems: 'center',
//     },
//   });