import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,
  
  TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const width = Dimensions.get('window').width / 2 - 23;



//plant is a props which is given in card component 

function BestValueItem({navigation}){
  

    const Card = ({plant}) => {

      return (
        <TouchableOpacity
          // activeOpacity={0.8}
          onPress={() => navigation.navigate('fruits', plant)}>
          <View style={styles.card}>
          <View style={{ flexDirection:'row',justifyContent:'space-between' }}>
            <View style={{backgroundColor:"#59A725",justifyContent:'center',borderRadius:5,width:"40%"}}>
              <Text style={{color:'white',alignSelf:'center'}}>5% OFF</Text>
            </View>



            
            <View
              style={{
                width: 30,
                height: 30,
                borderRadius: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: plant.like
                  ? 'rgba(245, 42, 42,0.2)'
                  : 'rgba(0,0,0,0.2) ',
              }}>
              <Icon
                name="heart"
                size={18}
                color={plant.like ? COLORS.red : COLORS.black}
              />
            </View>
          </View>

          <View
            style={{
              height: 100,
              alignItems: 'center',
            }}>
            <Image
              source={plant.img}
              style={{ flex: 1, resizeMode: 'contain' }}
            />
          </View>

          <Text style={{  fontSize: 17, marginTop: 5 }}>
            {plant.name}
          </Text>
          <Text style={{  fontSize: 17, marginTop: 5 }}>
            {plant.weight}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 5,
            }}>
            <Text style={{ fontSize: 19, fontWeight: 'bold' }}>
              ${plant.price}
            </Text>
       
          </View>
          <TouchableOpacity>
          <View style={{flexDirection:'row',justifyContent:"space-between",backgroundColor:'#E86225',width:'100%',padding:5,marginTop:4,borderRadius:4}}>
            <Text style={{color:'white',marginLeft:'30%'}}>Add</Text>
            <View style={{backgroundColor:"#C0521D",width:'20%',alignSelf:'flex-end'}}>
            <Text style={{color:'white',alignSelf:'center'}}>+</Text>
            </View>
          </View>
          </TouchableOpacity>
        </View>
        </TouchableOpacity>
      );
    };





    return(
    


              <SafeAreaView
              style={{flex: 1, paddingHorizontal: 15, backgroundColor: COLORS.light}}>

         
      
      <View style={styles.container}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        
       
     
      <Ionicons name="chevron-back-outline" size={30} color="#222" backgroundColor="white"  />
       
        </TouchableOpacity>
        <View style={styles.searchBar}>
          <Text style={{ fontSize: 22, fontWeight: 'bold',marginLeft:"30%" }}>Best Values</Text>

        </View>
        <View style={{  backgroundColor: 'orange', width: 40, height: 40, borderRadius: 150 / 2,alignSelf:'center',}}>
        <View style={styles.cart}>
         
          <Icon1 name="shoppingcart" size={30} color="white"  />
      
        </View>
        </View>
      </View>
    

              <FlatList
        columnWrapperStyle={{justifyContent: 'space-between'}}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          marginTop: 10,
          paddingBottom: 50,
        }}
        numColumns={2}
        data={CardData}
        renderItem={({item}) => {
          return <Card plant={item} />;
        }}
      />
       
      
       </SafeAreaView>
   
      
    )
}
export default BestValueItem;


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:5,
        // backgroundColor:COLORS.light
      
      },



    header: {
      marginTop: 30,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    searchContainer: {
      height: 50,
      backgroundColor: COLORS.light,
      borderRadius: 10,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },


     ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 5,
    bottom: 10,
    padding: 5,
    backgroundColor: 'black',
    // borderWidth:2,
    borderRadius: 5,



  },


  searchBar: {
    flexDirection: 'row',
    flex: 1,
    // borderWidth: 1,
    // borderColor: '#EFEFEF',
    // borderRadius: 8,
    // paddingHorizontal: 50,
    paddingVertical:10,
    fontSize: 50,
    alignItems: 'center',
    // marginHorizontal: 12,
    textAlign: 'center',
    alignSelf: 'center'
  },
  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },
  badge: {
    position: 'absolute',
    backgroundColor: '#E6848C',
    width: 18,
    height: 18,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    right: -4,
    top: -4,
  },
  badgeText: {
    color: '#fff',
  },
  cart: {
    marginTop:5,
    alignSelf:'center',
    // color:'white'
  
  
  },


  card: {
    height: 280,
    backgroundColor: COLORS.white,
    width,
    marginHorizontal: 2,
    borderRadius: 10,
    marginBottom: 20,
    padding: 15,
  },
  header: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },


  });