import React from 'react'
import { View, TextInput, Text, StyleSheet, Image } from 'react-native'
import { Feather } from '@expo/vector-icons'
import { Searchbar } from 'react-native-paper'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native';

const SearchBar = () => {
    const navigation = useNavigation();

    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);

    return (

        <View style={{ flexDirection: 'row' }}>
            <View style={{ marginLeft: 15, width: 355, marginTop: 15, }}>
        
                    <Searchbar
                        placeholder="Search for products"
                        onChangeText={() => navigation.navigate('Search')}
                        value={searchQuery}
                    />
             
            </View>

{/* 
            <View style={{
                alignItems: "center",
                // elevation:2,
                width: "15%",
                backgroundColor: "orange",

                height: 50,
                borderRadius: 10,

                justifyContent: "center",
                marginLeft: 10,
                marginTop: 15
            }}>
                <Image
                    source={require('../../assets/sort.png')}
                    style={{
                        width: 25, height: 18,
                    }}
                />
            </View> */}
        </View>

    )
}
export default SearchBar;
const styles = StyleSheet.create({
    backgroundStyle: {
        marginTop: 10,
        backgroundColor: '#FFFBF2',
        height: 50,
        width: '66%',
        borderRadius: 30,
        marginHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    textInput: {

        flex: 1,
        fontSize: 15

    },
    iconStyle: {
        fontSize: 25,
        alignSelf: 'center',
        color: '#878582'

    }

})

