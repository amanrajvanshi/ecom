import React, { component } from 'react'
import { View, Text, StyleSheet, SafeAreaView, FlatList, Image, Alert, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';

import StarRating from 'react-native-star-rating';
import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';



export default class Rivieww extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 4.5
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }


    render() {

        return (
            <View >
                <View style={styles.container}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>


                        <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                    </TouchableOpacity>
                    <View style={styles.searchBar}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 60, }}>Reviews</Text>

                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('RiviewEdit')}>
                        <View style={styles.cart}>

                            <FontAwesome name="edit" size={24} color="orange" />


                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{ borderWidth: 0.5, borderColor: '#D8D8D8', backgroundColor: 'white', height: 600, width: 380, alignSelf: 'center', marginTop: 20, borderRadius: 8 }}>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1 }}>
                            <Image source={require('../images/review.webp')}
                                style={{
                                    width: 75, height: 75, borderRadius: 37.5, marginTop: 20, marginLeft: 15
                                }} />
                        </View>


                        <View style={{ flex: 3 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ alignItems: 'center', }}>
                                    <Text style={{ fontSize: 18, marginTop: 30, color: 'black', fontWeight: 'bold', alignSelf: 'flex-start' }}>Milkey Licon</Text>
                                    <Text style={{ fontSize: 12, color: 'gray', }}>978,Reviews, 250 followers</Text>
                                </View>




                            </View>
                        </View>


                    </View>
                    <View style={{ borderWidth: 0.5, borderColor: '#D8D8D8', height: 42, width: 380, marginTop: 20 }}>
                        <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 8 }}>
                            <View style={{ marginLeft: 10 }}>
                                <StarRating
                                    disabled={false}
                                    emptyStar={'ios-star-outline'}
                                    fullStar={'ios-star'}
                                    halfStar={'ios-star-half'}
                                    iconSet={'Ionicons'}
                                    maxStars={5}
                                    rating={this.state.starCount}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'orange'}
                                    starSize={20}





                                />
                            </View>
                            <Text style={{ marginLeft: 10, fontSize: 15, fontWeight: 'bold' }}>3.0</Text>
                            <TouchableOpacity>
                                <View style={{ backgroundColor: 'orange', borderWidth: 0.5, borderColor: 'orange', height: 30, width: 30, borderRadius: 10, marginTop: -3, marginLeft: 140 }}>
                                    <Ionicons name="thumbs-up" size={15} color="white" style={{ alignSelf: 'center', marginVertical: 4 }} />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('RiviewEdit')}>
                                <View style={{ backgroundColor: '#67BC45', borderWidth: 0.5, borderColor: 'green', height: 30, width: 30, borderRadius: 10, marginTop: -3, marginRight: 10, }}>
                                    <View style={{ marginLeft: 3, marginTop: 3 }}>
                                        <Feather name="message-circle" size={20} color="white" />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ marginTop: 30, marginLeft: 10, marginRight: 10, fontSize: 17, color: 'gray' }}>ulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel ulla ut metus varius laoreet. Quisque rutrum.  ulla ut metus varius laoreet. Quisque rutrum.  augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipis  </Text>
                        <View style={{ flexDirection: "row", marginTop: 20, alignSelf: 'center' }}>
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, marginRight: 10, borderRadius: 5 }} />
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, marginRight: 10, borderRadius: 5 }} />
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, marginRight: 10, borderRadius: 5 }} />
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, borderRadius: 5 }} />
                        </View>
                        <View style={{ flexDirection: "row", marginTop: 20, alignSelf: 'center' }}>
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, marginRight: 10, borderRadius: 5 }} />
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, marginRight: 10, borderRadius: 5 }} />
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, marginRight: 10, borderRadius: 5 }} />
                            <Image source={require('../images/review.webp')} style={{ height: 80, width: 80, borderRadius: 5 }} />
                        </View>
                    </View>

                </View>



            </View>





        )
    }
}


const styles = StyleSheet.create({



    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        marginLeft: 13,
        marginRight: 13

    },

    searchBar: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: '#EFEFEF',
        // borderRadius: 8,
        paddingHorizontal: 60,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },
    badge: {
        position: 'absolute',
        backgroundColor: '#E6848C',
        width: 18,
        height: 18,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        right: -4,
        top: -4,
    },
    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },
    buttonStyle: {
        backgroundColor: '#8ebc0e',

        marginTop: 5,
        borderRadius: 10,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: 365,
        height: 50,
        alignSelf: 'center',
        marginTop: 50


    },
})
