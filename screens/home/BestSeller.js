import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,
  
  TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import BestSellerData from '../../assets/Data/BestSellerData'
import CardData from '../../CardData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const width = Dimensions.get('window').width / 2 - 20;
import { useNavigation } from '@react-navigation/native';



//plant is a props which is given in card component 

function BestSeller(){
  const navigation = useNavigation();
  

    const Card = ({BestSellerData}) => {

      return (
        <TouchableOpacity
          // activeOpacity={0.8}
          onPress={() => navigation.navigate('categoryitem', BestSellerData)}>
        <View style={styles.card}>
          <View style={{ flexDirection:'row',justifyContent:'space-between' }}>
            <View style={{backgroundColor:"#59A725",justifyContent:'center',borderRadius:5,width:"40%"}}>
              <Text style={{color:'white',alignSelf:'center'}}>5% OFF</Text>
            </View>



            
            <View
              style={{
                width: 25,
                height: 25,
                borderRadius: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: BestSellerData.like
                  ? 'rgba(245, 42, 42,0.2)'
                  : 'rgba(0,0,0,0.2) ',
              }}>
              <Icon
                name="heart"
                size={18}
                color={BestSellerData.like ? COLORS.red : COLORS.black}
              />
            </View>
          </View>

          <View
            style={{
              height: 100,
              alignItems: 'center',BestSellerData
            }}>
            <Image
              source={BestSellerData.img}
              style={{ flex: 1, resizeMode: 'contain',marginTop:"6%" }}
            />
          </View>

          <Text style={{  fontSize: 14, marginTop: 14 }}>
            {BestSellerData.name}
          </Text>
          <Text style={{  fontSize: 14, marginTop: 5 }}>
            {BestSellerData.weight}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 5,
            }}>
            <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
              ${BestSellerData.price}
            </Text>
       
          </View>
          <TouchableOpacity>
          <View style={{flexDirection:'row',justifyContent:"space-between",backgroundColor:'#E86225',width:'100%',marginTop:14,borderRadius:4}}>
            <Text style={{color:'white',marginLeft:'30%',padding:5,}}>Add</Text>
            <View style={{backgroundColor:"#C0521D",width:'20%',alignSelf:'flex-end'}}>
            <Text style={{color:'white',alignSelf:'center',padding:5, }}>+</Text>
            </View>
          </View>
          </TouchableOpacity>
        </View>
        </TouchableOpacity>
      );
    };





    return(
    


              <SafeAreaView
              style={{flex: 1, paddingHorizontal: 10, backgroundColor: COLORS.light}}>

         
    


<View style={{padding:16,flexDirection:'row',justifyContent:'space-between'}}>
                 <Text style={{fontSize:22,fontWeight:'bold',left:-14}}>Best Seller</Text>
               
             </View>
    

              <FlatList
        columnWrapperStyle={{justifyContent: 'space-between'}}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          marginTop: 10,
        //   paddingBottom: 50,
        }}
        numColumns={2}
        data={BestSellerData}
        renderItem={({item}) => {
          return <Card BestSellerData={item} />;
        }}
      />
       
      
       </SafeAreaView>
   
      
    )
}
export default BestSeller;


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:5,
        // backgroundColor:COLORS.light
      
      },



    header: {
      marginTop: 30,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    searchContainer: {
      height: 50,
      backgroundColor: COLORS.light,
      borderRadius: 10,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },


     ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 5,
    bottom: 10,
    padding: 5,
    backgroundColor: 'black',
    // borderWidth:2,
    borderRadius: 5,



  },


  searchBar: {
    flexDirection: 'row',
    flex: 1,
    // borderWidth: 1,
    // borderColor: '#EFEFEF',
    // borderRadius: 8,
    paddingHorizontal: 70,
    paddingVertical:10,
    fontSize: 50,
    alignItems: 'center',
    marginHorizontal: 12,
    textAlign: 'center',
    alignSelf: 'center'
  },
  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },
  badge: {
    position: 'absolute',
    backgroundColor: '#E6848C',
    width: 18,
    height: 18,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    right: -4,
    top: -4,
  },
  badgeText: {
    color: '#fff',
  },
  cart: {
    marginRight: 10
  },


  card: {
    height: 280,
    backgroundColor: COLORS.white,
    width,
    marginHorizontal: 5,
    borderRadius: 10,
    marginBottom: 20,
    padding: 15,
  },
  header: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },


  });