import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,
  
  TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import ShopData from '../../ShopData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ShopOfferItem from '../components/ShopOffersItem';
import GrabGoneData from '../../assets/Data/GrabGoneData'
const width = Dimensions.get('window').width / 3 -20;

import { useNavigation } from '@react-navigation/native';



//plant is a props which is given in card component 

function ShopOffer(){
  
  const navigation = useNavigation();
    const Card = ({GrabGoneData}) => {

      return (
        <TouchableOpacity
          // activeOpacity={0.8}
          onPress={() => navigation.navigate('fruits', GrabGoneData)}>
          <View style={styles.card}>
            <View style={{alignItems: 'flex-end'}}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: GrabGoneData.like
                    ? 'rgba(245, 42, 42,0.2)'
                    : 'rgba(0,0,0,0.2) ',
                }}>
                <Icon
                  name="heart"
                  size={18}
                  color={GrabGoneData.like ? COLORS.red : COLORS.black}
                />
              </View>
            </View>
  
              <Image
                source={GrabGoneData.img}
                style={styles.imgs}
                resizeMode="contain"
              />
         

            <Text style={{fontSize: 14,alignSelf:'center' }}>
              {GrabGoneData.name}
            </Text> 
            <Text style={{fontSize: 12,alignSelf:'center',marginTop:'30%',color:'green'}}>{GrabGoneData.discount}</Text>
          </View>
        </TouchableOpacity>
      );
    };





    return(
    


              <SafeAreaView
              style={{flex: 1, paddingHorizontal: 15, backgroundColor: COLORS.light}}>

         
    


              <View style={{padding:16,flexDirection:'row',justifyContent:'space-between'}}>
                 <Text style={{fontSize:22,fontWeight:'bold',left:-15}}>Grab or Gone</Text>
                
             </View>
    

              <FlatList
        columnWrapperStyle={{justifyContent: 'space-between'}}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          marginTop: 10,
          paddingBottom: 50,
        }}
        numColumns={3}
        data={GrabGoneData}
        renderItem={({item}) => {
          return <Card GrabGoneData={item} />;
        }}
      />
       
      
       </SafeAreaView>
   
      
    )
}
export default ShopOffer;


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:5,
        // backgroundColor:COLORS.light
      
      },



    header: {
      marginTop: 30,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    searchContainer: {
      height: 50,
      backgroundColor: COLORS.light,
      borderRadius: 10,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },


     ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 5,
    bottom: 10,
    padding: 5,
    backgroundColor: 'black',
    // borderWidth:2,
    borderRadius: 5,



  },


  searchBar: {
    flexDirection: 'row',
    flex: 1,
    // borderWidth: 1,
    // borderColor: '#EFEFEF',
    // borderRadius: 8,
    paddingHorizontal: 70,
    paddingVertical:10,
    fontSize: 50,
    alignItems: 'center',
    marginHorizontal: 12,
    textAlign: 'center',
    alignSelf: 'center'
  },
  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },
  badge: {
    position: 'absolute',
    backgroundColor: '#E6848C',
    width: 18,
    height: 18,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    right: -4,
    top: -4,
  },
  badgeText: {
    color: '#fff',
  },
  cart: {
    marginRight: 10
  },


  card: {
    height: 180,
    backgroundColor: COLORS.white,
    width,
    marginHorizontal: 2,
    borderRadius: 10,
    marginBottom: 20,
    padding: 15,
  },
  header: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imgs: {
    height: 70,
    width: 70,
    // marginTop:"2%"
    justifyContent: 'center',
    alignSelf: 'center'
},


  });