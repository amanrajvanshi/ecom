import React,{useState} from 'react'
import {View ,Text,StyleSheet, Image,ImageBackground,ScrollView,TouchableOpacity,FlatList,TextInput} from 'react-native'

import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';


function FreshPickup(){

    const image = { uri: "https://hotdealszone.com/wp-content/uploads/2020/09/Flipkart-Big-Billion-Days-2020-Coming-Soon-1024x574.jpg" };

 const [gallery, setgallery] = useState([
        { image: {uri: 'https://zestyvibe.com/wp-content/uploads/2020/05/organic_vegetables_basket.jpg'}, title: 'Order Now',  key: '1' },
        { image:{uri:'https://zestyvibe.com/wp-content/uploads/2020/05/organic_vegetables_basket.jpg'}, title: 'Order Now',key: '2' },
        { image:{uri:'https://zestyvibe.com/wp-content/uploads/2020/05/organic_vegetables_basket.jpg'}, title: 'Order Now',key: '3' },
        { image:{uri:'https://zestyvibe.com/wp-content/uploads/2020/05/organic_vegetables_basket.jpg'}, title: 'Order Now', key: '4' },
      ]);
      const navigation = useNavigation();
    return(
        <View>
         <ScrollView>
             <View style={{padding:16 ,flexDirection:'row',justifyContent:'space-between'}}>
                 <Text style={{fontSize:22,fontWeight:'bold'}}>Fresh Pickups </Text>
               
             </View>
             <View>
                 <FlatList
                 horizontal={true}
                 data ={gallery}
                 renderItem={({item})=>{
                     return(
                         <View style={{paddingLeft:16}}>
                             <TouchableOpacity   onPress={() => navigation.navigate('categoryitem')} >
                          <Image source={item.image} style={{width:150,height:200,borderRadius:10,marginRight:8}}/>
                          <Text style={styles.ImageText}>{item.title}</Text>
                             </TouchableOpacity>
                            
                         </View>
                     )
                 }}
                 
                 
                 />
                       <View style={{marginTop:19}}>
                     <TouchableOpacity>
            <ImageBackground
            source={image}
            style={{width:'97%',height:200,marginLeft:15,mariginTop:10}}
            imageStyle ={{borderRadius:10,}}
            >

            </ImageBackground>
            </TouchableOpacity>
        

                 </View>
             </View>
         </ScrollView>
     

        </View>
    )
}
export default FreshPickup;





const styles = StyleSheet.create({

    ImageText:{
        position:'absolute',
        color:'white',
        marginTop:4,
        fontSize:14,
        left:5,
        bottom:10,
        padding:5,
        backgroundColor:'black',
        // borderWidth:2,
        borderRadius:5,
        


    }
})