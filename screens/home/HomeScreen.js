import React from 'react'
import { View, Button, Image, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native'
import Icon1 from 'react-native-vector-icons/AntDesign';

import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Text,
  TouchableRipple,
  Switch,
  Drawer,
  // SafeAreaView,
  FlatList

} from 'react-native-paper';
import SearchBar from '../components/SearchBar'
import { Searchbar } from 'react-native-paper'
import Category from './Category'
import BestValue from './BestValue'
import BestSeller from './BestSeller'
import { ScrollView } from 'react-native-gesture-handler'
import ShopOffer from './ShopOffer'
import FreshPickup from './FreshPickup'
import GrabOrGone from './GrabOrGone'
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

function HomeScreen() {
  const navigation = useNavigation();

  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);
  return (
    <SafeAreaView style={{ backgroundColor: '#F3F4F8', flex: 1, }}>
      <View style={{
        backgroundColor: 'black',
        paddingHorizontal:10
        // 
      }}>
        <View style={{
          // flex:1,
          flexDirection: "row",
// justifyContent:'space-between',S
          // width: "100%",
          marginTop: 10,
        
        }}>


          <View style={{}}>

            <Ionicons name='menu-outline' size={43}
              backgroundColor="#F4F5F9" color='white' onPress={() => navigation.openDrawer()} />
          </View>
<TouchableOpacity onPress={() => navigation.navigate("Location")}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{ color: 'lightgrey' }}>Your Location</Text>
            <View style={{flexDirection:'row'}}>
            <Text style={{ fontSize: 16, color: 'white' }}>Cannaught place,New Delhi</Text>
            <Ionicons name='chevron-down-outline' color={"white"} size={20} />
            </View>
          </View>
          </TouchableOpacity>
          <View style={{marginTop:10,marginLeft:"25%"}}>
            <TouchableOpacity>
          <Icon1 name='shoppingcart' color={"white"} size={30}/>
          </TouchableOpacity>
          </View>

        </View>
        <View style={styles.backgroundStyle}>
          <View style={{ flexDirection: 'row',justifyContent:'space-between',paddingBottom:10}}>
       
          <View style={{
              alignItems: "center",
             
           
              width: "22%",
              backgroundColor: "white",
             
          
              borderRadius: 5,
             
             
             
            }}>
              <TouchableOpacity onPress={() => navigation.navigate('headcategory')}>
              <Text style={{marginTop:15,fontSize:14,fontalignContent:'center',fontWeight:'bold'}}>Categories</Text>
              </TouchableOpacity>
            </View>
           
            <Searchbar
              placeholder="Search for products"
              onChangeText={onChangeSearch}
              value={searchQuery}
              style={{width:"76%"}}
            />

          
          </View>
        </View>
      </View>

      <ScrollView>


        <BestValue />
        <Category />
        <BestSeller />
        <ShopOffer />
        <FreshPickup />
        <GrabOrGone />

      </ScrollView>

    </SafeAreaView>
  )
}
export default HomeScreen;



const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
    backgroundColor: '#678E21'

  },
  userInfoSection: {
    // paddingLeft: 20,
  },

  caption: {
    fontSize: 5,
    //   lineHeight: 6,
    color: 'black'
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    // marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    // marginRight: 3,
    color: '#F4F5F9'
  },
  drawerSection: {
    marginTop: 15,


  },
  bottomDrawerSection: {

    borderTopColor: '#f4f4f4',
    backgroundColor: '#678E21'

  },
  locationTitle: {
    fontSize: 13,

    fontWeight: 'bold',
    color: 'black'
  },

  textInput: {

    flex: 1,
    fontSize: 15

  },
  iconStyle: {
    fontSize: 25,
    alignSelf: 'center',
    color: '#878582'

  }

});