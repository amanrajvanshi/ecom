import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,
  
  TouchableOpacity
} from 'react-native';
import COLORS from '../../assets/Color';
import ShopData from '../../ShopData'
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ShopOfferItem from '../components/ShopOffersItem';
const width = Dimensions.get('window').width / 3 -20;
import { useNavigation } from '@react-navigation/native';


//plant is a props which is given in card component 

function ShopOffer({navigation}){
  

    const Card = ({plant}) => {

      const navigation = useNavigation();
      return (
        <TouchableOpacity
          // activeOpacity={0.8}
          onPress={() => navigation.navigate('categoryitem', plant)}>
          <View style={styles.card}>
            {/* <View style={{alignItems: 'flex-end'}}>
              <View
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: plant.like
                    ? 'rgba(245, 42, 42,0.2)'
                    : 'rgba(0,0,0,0.2) ',
                }}>
                <Icon
                  name="heart"
                  size={18}
                  color={plant.like ? COLORS.red : COLORS.black}
                />
              </View>
            </View> */}
  
              <Image
                source={plant.img}
                style={{height:100,width:100,justifyContent:'center',alignSelf:'center',resizeMode:'contain'}}
              />
            {/* </View> */}
             {/*   
            <Text style={{fontWeight: 'bold', fontSize: 17, marginTop: 10}}>
              {plant.name}
            </Text> */}
            {/* <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{fontSize: 19, fontWeight: 'bold'}}>
                ${plant.price}
              </Text>
              <View
                style={{
                  height: 25,
                  width: 25,
                  backgroundColor: COLORS.orange,
                  borderRadius: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                 <Icon
                  name="cart-outline"
                  size={18}
                  color={plant.like ? COLORS.white : COLORS.white}
                />
              </View>
            </View> */}
          </View>
        </TouchableOpacity>
      );
    };





    return(
    


              <SafeAreaView
              style={{flex: 1, paddingHorizontal: 15, backgroundColor: COLORS.light}}>

         
    


<View style={{padding:16,flexDirection:'row',justifyContent:'space-between'}}>
                 <Text style={{fontSize:22,fontWeight:'bold',left:-10}}>Shop By Offer</Text>
               
             </View>
    

              <FlatList
        columnWrapperStyle={{justifyContent: 'space-between'}}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          marginTop: 10,
        //   paddingBottom: 50,
        }}
        numColumns={3}
        data={ShopData}
        renderItem={({item}) => {
          return <Card plant={item} />;
        }}
      />
       
      
       </SafeAreaView>
   
      
    )
}
export default ShopOffer;


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:5,
        // backgroundColor:COLORS.light
      
      },



    header: {
      marginTop: 30,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    searchContainer: {
      height: 50,
      backgroundColor: COLORS.light,
      borderRadius: 10,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    },


     ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 5,
    bottom: 10,
    padding: 5,
    backgroundColor: 'black',
    // borderWidth:2,
    borderRadius: 5,



  },


  searchBar: {
    flexDirection: 'row',
    flex: 1,
    // borderWidth: 1,
    // borderColor: '#EFEFEF',
    // borderRadius: 8,
    paddingHorizontal: 70,
    paddingVertical:10,
    fontSize: 50,
    alignItems: 'center',
    marginHorizontal: 12,
    textAlign: 'center',
    alignSelf: 'center'
  },
  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },
  badge: {
    position: 'absolute',
    backgroundColor: '#E6848C',
    width: 18,
    height: 18,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    right: -4,
    top: -4,
  },
  badgeText: {
    color: '#fff',
  },
  cart: {
    marginRight: 10
  },


  card: {
    height: 145,
    backgroundColor: COLORS.white,
    width,
    marginHorizontal: 2,
    borderRadius: 10,
    marginBottom: 20,
    padding: 15,
  },
  header: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },


  });