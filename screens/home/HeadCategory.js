import React, { useState } from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,

    TouchableOpacity

} from 'react-native';
// import COLORS from '../../assets/Color';
// import CardData from '../../CardData'
// import MyOrderData from '../../assets/Data/MyOrderData'
// import { Ionicons } from '@expo/vector-icons';
import { Searchbar } from 'react-native-paper'
import DropDownItem from "react-native-drop-down-item";
import ic_arr_down from '../../assets/ic_arr_down.png';
import ic_arr_up from '../../assets/ic_arr_up.png';
import COLORS from '../../assets/Color';
import MyOrderData from '../../assets/Data/MyOrderData'
import { Ionicons } from '@expo/vector-icons';
import ShopData from '../../ShopData'
import Icon1 from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const width = Dimensions.get('window').width / 3 - 11;



//plant is a props which is given in card component 

function CategoryItem({ navigation }) {

    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);


    // card component of dorpdown

    const Card = ({ plant }) => {


        return (

            <View>

                <DropDownItem
                    // key={i}
                    style={styles.dropDownItem}
                    contentVisible={false}
                    useNativeDriver={true}
                    invisibleImage={ic_arr_down}
                    visibleImage={ic_arr_up}
                    style={styles.invisibleImage}
                    style={styles.visibleImage}
                    header={


                        <View style={styles.item}>






                            <Image source={plant.img} resizeMode="contain" style={styles.imgs} />
                            <View style={{ flexDirection: 'column', marginLeft: 30, flex: 1, justifyContent: 'center', marginTop: "2%", }}>
                                {/* <Text >{plant.order}</Text> */}

                                <Text >Rs {plant.price}</Text>
                                <Text style={{ color: 'red' }}>Rs {plant.sumery}</Text>
                                {/* <Text >Rs {plant.date}</Text> */}



                                <Text style={{ color: 'gray', fontWeight: 'bold' }}>{plant.quantity}</Text>

                            </View>

                        </View>


                    }

                >

                    {/* FlatList of inside dropdowm screen  */}

                    <View>

                        <FlatList
                            columnWrapperStyle={{ justifyContent: 'space-between', }}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{
                                marginTop: 10,
                                //   paddingBottom: 50,
                            }}
                            numColumns={3}
                            data={ShopData}
                            renderItem={({ item }) => {
                                return <Cards plant={item} />;
                            }}

                        />
                    </View>
                </DropDownItem>


                <View style={{ height: 10 }} />
            </View>






        );
    };


    // inside card component of dropdowm


    const Cards = ({ plant }) => {

        // const navigation = useNavigation();
        return (
            <TouchableOpacity
                // activeOpacity={0.8}
                onPress={() => navigation.navigate('fruits', plant)}>
                <View style={{ flex: 1, }}>
                    <View style={styles.card}>

                        <Image
                            source={plant.img}
                            style={styles.imgs}
                            resizeMode="contain"
                        />
                        <Text style={{ fontSize: 13, alignSelf: 'center' }} >
                            {plant.name}
                        </Text>
                       
                            <Text style={{ fontSize: 13, alignSelf: 'center',marginTop:"15%",color:'green' }}>
                                {plant.discount}
                            </Text>
                   

                    </View>
                </View>
            </TouchableOpacity>
        );
    };






    // return area



    return (



        <SafeAreaView
            style={{ flex: 1, backgroundColor: COLORS.light, marginBottom: "20%" }}>



            <View style={{
                backgroundColor: 'black',
                paddingHorizontal: 10
                // 
            }}>
                <View style={{
                    // flex:1,
                    flexDirection: "row",

                    // width: "100%",
                    marginTop: 10,

                }}>


                    <View style={{}}>

                        <Ionicons name='menu-outline' size={43}
                            backgroundColor="#F4F5F9" color='white' onPress={() => navigation.openDrawer()} />
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate("Location")}>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ color: 'lightgrey' }}>Your Location</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16, color: 'white' }}>Cannaught place,New Delhi</Text>
                                <Ionicons name='chevron-down-outline' color={"white"} size={20} />
                            </View>
                        </View>
                    </TouchableOpacity>

                </View>
                <View >
                    <View style={{ paddingBottom: 10 }}>

                        <Searchbar
                            placeholder="Search for products"
                            onChangeText={onChangeSearch}
                            value={searchQuery}
                        // style={{ width: "76%" }}
                        />


                    </View>
                </View>
            </View>

            {/* FlatList of first show screen  */}

            <View style={{ marginHorizontal: 10 }}>


                <FlatList

                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        marginTop: 10,
                        // marginBottom: 50,
                    }}

                    data={MyOrderData}
                    renderItem={({ item }) => {
                        return <Card plant={item} />;
                    }}
                />
            </View>
            <View>


            </View>

        </SafeAreaView>


    )
}
export default CategoryItem;


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        // backgroundColor:COLORS.light

    },



    header: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    searchContainer: {
        height: 50,
        backgroundColor: COLORS.light,
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },




    searchBar: {
        flexDirection: 'row',
        flex: 1,

        paddingHorizontal: 70,
        paddingVertical: 10,
        fontSize: 50,
        alignItems: 'center',
        marginHorizontal: 12,
        textAlign: 'center',
        alignSelf: 'center'
    },
    input: {
        fontSize: 18,
        marginLeft: 10,
        paddingVertical: 5,
    },

    badgeText: {
        color: '#fff',
    },
    cart: {
        marginRight: 10
    },



    header: {
        // marginTop: 30,
        flexDirection: 'row',
        // justifyContent: 'space-between',
    },

    img: {
        height: 70,
        width: 70
    },
    item: {
        flexDirection: 'row',

        backgroundColor: "white",
        shadowColor: "#000",

        textShadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,


        borderRadius: 10,


    },
    imgs: {
        height: 70,
        width: 70,
        // marginTop:"2%"
        justifyContent: 'center',
        alignSelf: 'center'
    },
    card: {
        height: 140,
        backgroundColor: COLORS.white,
        width,
        // padding:5,
        marginHorizontal: 8,
        // padding:20,
        borderRadius: 10,
        marginBottom: 5,
        // padding: 15,
        marginLeft: -7
    },
    visibleImage: {
        //  marginTop:10
    },
    invisibleImage: {
        // marginTop:10

    }





});