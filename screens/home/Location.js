import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, TextInput, CheckBox, Image } from 'react-native';
import COLORS from '../../assets/Color';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
import { Searchbar } from 'react-native-paper'
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';



const Location = () => {
    const navigation = useNavigation();
    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);




    return (



        <View style={{paddingHorizontal:15}}>
            <View style={styles.container}>
                <View style={{}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Home')}>


                        <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

                    </TouchableOpacity>
                </View>

                <View style={styles.searchBar}>
                    <View style={{ width: "90%", marginTop: 10, alignSelf: 'center', marginLeft: 20, color: 'red' }}>

                        <Searchbar
                            placeholder="Search for area,street "
                            onChangeText={onChangeSearch}
                            value={searchQuery}
                            style={{ backgroundColor: 'lightgrey' }}
                        />

                    </View>

                </View>


            </View>

            <View style={{ paddingHorizontal: 5, flexDirection: 'row', backgroundColor: COLORS.light, height: 70, marginTop: 10, borderRadius: 10 }}>
                <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29, marginTop: 10 }}>
                    <Icon name="crosshairs-gps" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                </View>
                
                <View style={{ borderBottomWidth: 1, width: "85%", marginLeft: 10, flexDirection: 'column' }}>
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 10,
                            fontWeight: 'bold',
                            // marginLeft: 5,


                        }}>
                        Amit Patel

                            </Text>
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 12,
                            lineHeight: 22,
                            // marginTop: 19,
                            fontWeight: 'bold',
                            // marginLeft: 5,


                        }}>
                        using GPS

                            </Text>
                </View>
            </View>
         

            <View style={{paddingHorizontal:40,marginTop:20,}}>
                <Text style={{fontSize:16,color:COLORS.dark}}>
                Saved Address
                </Text>
            </View>

            <TouchableOpacity>
            <View style={{ paddingHorizontal: 5, flexDirection: 'row', backgroundColor: COLORS.light, height: 70, marginTop: 10, borderRadius: 10 }}>
                <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29, marginTop: 10 }}>
                    <Icon name="home" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                </View>
                <View style={{ borderBottomWidth: 1, width: "85%", marginLeft: 10, flexDirection: 'column' }}>
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 10,
                            fontWeight: 'bold',
                            // marginLeft: 5,


                        }}>
                        Amit Patel

                            </Text>
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 12,
                            lineHeight: 22,
                            // marginTop: 19,
                            fontWeight: 'bold',
                            // marginLeft: 5,


                        }}>
                        Village kiratpur Rudrapur Uttrakhand (US nagar)

                            </Text>
                </View>
            </View>
            </TouchableOpacity>



            <TouchableOpacity>     
            <View style={{ paddingHorizontal: 5, flexDirection: 'row', backgroundColor: COLORS.light, height: 70, marginTop: 10, borderRadius: 10 }}>
                <View style={{ backgroundColor: COLORS.white, borderRadius: 100, width: 29, height: 29, marginTop: 10 }}>
                    <Icon name="map-marker" size={25} style={{ alignSelf: 'center', color: COLORS.orange }} />
                </View>
                <View style={{ borderBottomWidth: 1, width: "85%", marginLeft: 10, flexDirection: 'column' }}>
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 15,
                            lineHeight: 22,
                            marginTop: 10,
                            fontWeight: 'bold',
                            // marginLeft: 5,


                        }}>
                        Amit Patel

                            </Text>
                    <Text
                        style={{
                            color: 'black',
                            fontSize: 12,
                            lineHeight: 22,
                            // marginTop: 19,
                            fontWeight: 'bold',
                            // marginLeft: 5,


                        }}>
                        Village kiratpur Rudrapur Uttrakhand (US nagar)

                            </Text>
                </View>
            </View>

            </TouchableOpacity>


        </View>














    );

}







export default Location;

const styles = StyleSheet.create({




    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
        // paddingHorizontal: 15,
        // marginLeft: 8,
        // marginRight: 8,
        justifyContent: 'space-evenly',
        // backgroundColor:"transparent",

    },





    searchBar: {
        flexDirection: 'row',
        flex: 1,


        fontSize: 50,

    },


})