
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import Icon1 from 'react-native-vector-icons/AntDesign';



const HomeStack = createStackNavigator();
const OrderStack = createStackNavigator();
const SearchStack = createStackNavigator();
const FavouriteStack = createStackNavigator();
const UserStack = createStackNavigator();


const Tab = createBottomTabNavigator();





import HomeScreen from '../screens/home/HomeScreen'

import SearchScreen from '../screens/search/SerachScreen';
import OrderScreen from "../screens/order/OrderScreen";
import UserScreen from '../screens/user/UserScreen'

import CategoryItem from '../screens/components/CategoryItem';
import FreshPickupItem from '../screens/components/FreshPickupItem';
import GrabOrGoneItem from '../screens/components/GrabOrGoneItem';
import BestSellerItem from '../screens/components/BestSellerItem';
import BestValueItem from '../screens/components/BestValueItem';
import ShopOfferItem from '../screens/components/ShopOffersItem'
import Fruits from '../screens/components/Fruit';
import SingleItem from '../screens/components/SingleItem';

import CheckoutScreen from '../screens/order/Checkout.screen';
import DeliveryAddress from '../screens/order/DeliveryAddress';
import OrderSuccess from '../screens/order/OrderSuccess'
import TrackOrder from '../screens/order/TrackOrder';
import AddNewAdrs from '../screens/order/AddNewAdrs';

import AboutMe from '../screens/user/AboutMe';


import RiviewEdit from '../screens/components/RiviewEdit';
import Rivieww from '../screens/components/Rivieww';

import MyOrder from '../screens/order/Myorder';

// import FavouriteScreen from '../screens/favourite/FavouriteScreen';
import OrderDetail from '../screens/order/OrderDetail';
import FavouriteScreen from '../screens/favourite/FavouriteScreen';

import NewAdrs from '../screens/user/NewAddress';
import PaymentScreen from '../screens/order/PaymentScreen';

import Location from '../screens/home/Location';
import HeadCategory from '../screens/home/HeadCategory'








// all Tab navigator with calling of stack navigator 



const MainTabScreen = () => (
  <Tab.Navigator
    initialRouteName="Home"
    activeColor="#F4A921"
    barStyle={{ backgroundColor: '#F4F5F9' }}
    headerTitleStyle={
      fontWeight = 'bold'
    }
    tabBarOptions={{
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    }}

  >
    <Tab.Screen
      name="Search"
      component={SearchStackScreen}
      options={{
        labelStyle: {
    fontSize: 30,
    margin: 0,
   padding: 0,
   },
        
        tabBarLabel: 'Search',
        tabBarIcon: ({ color }) => (
          <Ionicons name="search-outline" color={color} size={26}
           />
           
        ),
//         tabBarOptions: {
//   labelStyle: {
//     fontSize: 20,
//     margin: 0,
//     padding: 0,
//   },
// }

  
      }}
      
    />
    <Tab.Screen
      name="Favourite"
      component={FavouriteStackScreen}
      options={{

        tabBarLabel: 'Favourite',
        tabBarIcon: ({ color }) => (
          <Ionicons name="heart-outline" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({ color }) => (
          <Ionicons name="home-outline" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Order"
      component={OrderStackScreen}
      options={{
        tabBarLabel: 'Cart',
        tabBarIcon: ({ color }) => (
          <Icon1 name="shoppingcart" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="User"
      component={UserStackScreen}
      options={{
        tabBarLabel: 'user',
        tabBarIcon: ({ color }) => (
          <Ionicons name="person-outline" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
)
export default MainTabScreen;



// all stack navigator 


const SearchStackScreen = ({ navigation }) => (

  <SearchStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#F4F5F9',

    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold'
    }
  }}>
    <SearchStack.Screen name="Search" component={SearchScreen} options={{
      headerShown: false,
      title: 'Search',
      color: 'black',
      headerLeft: () => (
        <Ionicons.Button name='menu' size={25}
          backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

      ),
      headerRight: () => (
        <Ionicons.Button name='cart-outline' size={25}
          backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

      )
    }} />

  </SearchStack.Navigator>
)



const FavouriteStackScreen = ({ navigation }) => (

  <FavouriteStack.Navigator screenOptions={{
    headerShown: false,
    headerStyle: {
      backgroundColor: '#F4F5F9'
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold'
    }

  }}>

    <FavouriteStack.Screen name="favourite" component={FavouriteScreen}

      options={{
        title: 'Favourite',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

        ),
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}



    />
  </FavouriteStack.Navigator>
)

const HomeStackScreen = ({ navigation }) => (

  <HomeStack.Navigator screenOptions={{

    headerStyle: {
      backgroundColor: '#F4F5F9'
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold'
    }

  }}>



    <HomeStack.Screen name="Home" component={HomeScreen}

      options={{

        title: 'Home',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

        ),
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}



    />

    {/* Best vlaue Item stack screen  */}


    <HomeStack.Screen name="bestvalueitem" component={BestValueItem}

      options={{
        title: 'Best Value Item',
        headerShown: false,

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />


    {/* //   category Listitem StackScreen  */}


    <HomeStack.Screen name="categoryitem" component={CategoryItem}

      options={{
        title: 'Category',
        headerShown: false,

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}

    />

    <HomeStack.Screen name="Riview" component={Rivieww}

      options={{
        title: 'Category',
        headerShown: false,

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}

    />


    <HomeStack.Screen name="RiviewEdit" component={RiviewEdit}

      options={{
        title: 'Category',
        headerShown: false,

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}

    />


    <HomeStack.Screen name="Location" component={Location}

      options={{
        title: 'Category',
        headerShown: false,

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}

    />





    {/* Best Seller StackScreen  */}



    <HomeStack.Screen name="bestselleritem" component={BestSellerItem}

      options={{
        title: 'Best Selling Items',

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />


    {/* Fresh pickup StackScreen*/}




    <HomeStack.Screen name="freshitem" component={FreshPickupItem}

      options={{
        title: 'Fresh Items',

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />

    {/* GrabOrGone StackScreen  */}



    <HomeStack.Screen name="grabitem" component={GrabOrGoneItem}

      options={{
        title: 'Grab Or Gone',

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />

    {/* ShopOffer StackScreen */}

    <HomeStack.Screen name="shopitem" component={ShopOfferItem}

      options={{
        title: 'Offer',

        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}

    //  Fruits StackScreen

    />
    <HomeStack.Screen name="Item" component={SingleItem}

      options={{
        title: 'fruits',
        headerShown: false,
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />

    {/* add to cart stack screen  */}

    <HomeStack.Screen name="fruits" component={Fruits}

      options={{
        title: 'fruits',
        headerShown: false,
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />

    {/* header category item StackScreen */}

    <HomeStack.Screen name="headcategory" component={HeadCategory}

options={{
  title: 'fruits',
  headerShown: false,
  headerRight: () => (
    <Ionicons.Button name='cart-outline' size={25}
      backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

  )
}}
/>





  </HomeStack.Navigator>
)



const OrderStackScreen = ({ navigation }) => (

  <OrderStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#F4F5F9'
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold'
    }

  }}>

    <OrderStack.Screen name="ShoppingCart" component={OrderScreen}

      options={{
        title: 'Order',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#009837" onPress={() => navigation.openDrawer()}></Ionicons.Button>

        )
      }}
    />

<OrderStack.Screen name="payment" component={PaymentScreen}

options={{
  title: 'Order',
  headerShown: false,
  headerLeft: () => (
    <Ionicons.Button name='menu' size={25}
      backgroundColor="#009837" onPress={() => navigation.openDrawer()}></Ionicons.Button>

  )
}}
/>


    <OrderStack.Screen name="checkout" component={CheckoutScreen}

      options={{
        title: 'Order',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

        ),
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />

    <OrderStack.Screen name="DeliveryAddress" component={DeliveryAddress}

      options={{
        title: 'Order',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#009837" onPress={() => navigation.openDrawer()}></Ionicons.Button>

        )
      }}
    />

    <OrderStack.Screen name="Order Success" component={OrderSuccess}

      options={{
        title: 'Order',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#009837" onPress={() => navigation.openDrawer()}></Ionicons.Button>

        )
      }}
    />

    <OrderStack.Screen name="Track Order" component={TrackOrder}

      options={{
        title: 'Order',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#009837" onPress={() => navigation.openDrawer()}></Ionicons.Button>

        )
      }}
    />

    <OrderStack.Screen name="AddNewAdrs" component={AddNewAdrs}

      options={{
        title: 'Order',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#009837" onPress={() => navigation.openDrawer()}></Ionicons.Button>

        )
      }}
    />


    {/* <OrderStack.Screen name="myorder" component={MyOrder}

options={{
  title: 'Order',
  headerShown: false,
  headerLeft: () => (
    <Ionicons.Button name='menu' size={25}
      backgroundColor="#009837" onPress={() => navigation.openDrawer()}></Ionicons.Button>

  )
}}
/> */}




  </OrderStack.Navigator>


)



const UserStackScreen = ({ navigation }) => (

  <UserStack.Navigator screenOptions={{
    headerStyle: {
      backgroundColor: '#F4F5F9'
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold'
    }

  }}>

    <UserStack.Screen name="profile" component={UserScreen}

      options={{
        title: 'User',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

        ),
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />

    <UserStack.Screen name="about" component={AboutMe}

      options={{
        title: 'User',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

        ),
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />


    <UserStack.Screen name="myorder" component={MyOrder}

      options={{
        title: 'User',
        headerShown: false,
        headerLeft: () => (
          <Ionicons.Button name='menu' size={25}
            backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

        ),
        headerRight: () => (
          <Ionicons.Button name='cart-outline' size={25}
            backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

        )
      }}
    />





<UserStack.Screen name="orderDetail" component={OrderDetail}

options={{
  title: 'User',
  headerShown: false,
  headerLeft: () => (
    <Ionicons.Button name='menu' size={25}
      backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

  ),
  headerRight: () => (
    <Ionicons.Button name='cart-outline' size={25}
      backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

  )
}}
/>
<UserStack.Screen name="newadd" component={NewAdrs}

options={{
  title: 'User',
  headerShown: false,
  headerLeft: () => (
    <Ionicons.Button name='menu' size={25}
      backgroundColor="#F4F5F9" color='black' onPress={() => navigation.openDrawer()}></Ionicons.Button>

  ),
  headerRight: () => (
    <Ionicons.Button name='cart-outline' size={25}
      backgroundColor="#F4F5F9" color='black'></Ionicons.Button>

  )
}}
/>





  </UserStack.Navigator>
)