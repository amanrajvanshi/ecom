

import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import OnboardingScreen from '../bordingscreen/OnboardingScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NumberScreen from '../bordingscreen/NumberScreen';
import OtpScreen from '../bordingscreen/OtpScreen';
import NameScreen from '../bordingscreen/NameScreen';


const AppStack = createStackNavigator();

const RootStackScreen = () => {

  return (

    //   <NavigationContainer> 
    <AppStack.Navigator



    >
      <AppStack.Screen options={{ headerShown: false }} name="Onboarding" component={OnboardingScreen}
      />
      <AppStack.Screen name="Verify Phone Number" component={NumberScreen} options={{
        headerShown: false
      }} />
      <AppStack.Screen name="Phone Verification" component={OtpScreen} options={{headerStyle:{backgroundColor:'#fff6e6',elevation:0,  shadowColor: 'transparent'},headerTintColor:'black', headerTitleStyle: {marginLeft: 55},  headerShown: false,}} />
      <AppStack.Screen name="Name" component={NameScreen} options={{headerStyle:{backgroundColor:'#fff6e6',elevation:0,  shadowColor: 'transparent'},headerTintColor:'black', headerTitleStyle: {marginLeft: 55},  headerShown: false,}} />
    </AppStack.Navigator>
    // </NavigationContainer>

  )



}









export default RootStackScreen;