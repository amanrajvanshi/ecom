import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, TextInput, phoneNumber } from 'react-native';
import PhoneInput from "react-native-phone-number-input";
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import DropDownPicker from 'react-native-dropdown-picker';


const NumberScreen = ({navigation}) => {
  // const navigation = useNavigation();







  return (



    <View style={{ flex: 1 }}>
      {/* <ImageBackground source={require('../assets/index1.jpg')} */}


      <LinearGradient
        // Background Linear Gradient
        colors={['rgba(198,227,119,0.5)', 'transparent']}
        style={styles.linearGradient}
      >

        {/* style={{flex: 1, width: '100%', resizeMode: "contain" }}> */}


        <Text style={{ fontSize: 18, alignSelf: 'center', marginTop: 15, fontWeight: 'bold' }}>Verify Phone Number</Text>
        <Text style={styles.text1}>We have sent you on sms with a code{"\n"}
    to number +917988382769</Text>
        <View style={{ flexDirection: 'row', paddingHorizontal: 12, borderRadius: 10, alignItems: 'center', borderWidth: 1, borderColor: 'gray', width: 330, alignSelf: 'center', marginTop: 30, height: 55 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <Text>+91</Text>

          </View>
          <View >
            <TextInput style={{ marginLeft: 10, flex: 1, height: 50, maxWidth: '100%' }}
              keyboardType='numeric'
              maxLength={10}



            />

          </View>
        </View>





        <TouchableOpacity style={styles.buttonStyle}
          onPress={() => navigation.navigate('Phone Verification')}

        >
          <Text style={styles.text2}>CONTINUE</Text>
        </TouchableOpacity>

      </LinearGradient>

    </View>

  );

}

const styles = StyleSheet.create({

  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },

  text: {
    fontSize: 23,
    marginLeft: 20,
    marginBottom: 10,
    marginTop: 20,
    color: 'black',


  },


  text1: {
    fontSize: 16,
    marginLeft: 34,
    marginTop: 40,
    marginBottom: 10,
    color: 'gray',
  },
  text2: {

    color: 'white',
  },


  buttonStyle: {
    backgroundColor: '#8ebc0e',

    marginTop: 250,
    borderRadius: 10,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 330,
    height: 55,
    alignSelf: 'center',


  },




});
export default NumberScreen;