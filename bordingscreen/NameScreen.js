import React from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import Icon1 from 'react-native-vector-icons/AntDesign';
import { AuthContext } from '../Autantication/Context'
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
const NameScreen = () => {
  const navigation = useNavigation();
  const [data, setData] = React.useState({
    signIn: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true

  });
  const { signIn } = React.useContext(AuthContext);
  const textInputChange = (val) => {
    if (val.length != 0) {
      setData({
        ...data,
        username: val,
        check_textInputChange: true
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false
      })
    }
  }


  const handlePasswordChange = (val) => {
    setData({
      ...data,
      password: val

    })
  }
  const updateScureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry
    })
  }



  return (
    <View style={{ flex: 1 }}>
      <LinearGradient
        // Background Linear Gradient
        colors={['rgba(198,227,119,0.5)', 'transparent']}
        style={styles.linearGradient}
      >


        <View style={styles.container}>

          <View style={styles.searchBar}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', alignSelf: 'center' }}>Personal Detail</Text>

          </View>
          <TouchableOpacity onPress={() => { signIn() }}>




          </TouchableOpacity>







        </View>
        <View style={{ marginTop: 40, }}>
          <View style={{ borderWidth: 1, borderColor: 'gray', height: 65, width: 340, alignSelf: 'center', borderRadius: 8 }}>
            <Text style={{ marginTop: 7, paddingLeft: 15, color: 'black' }}> Full Name</Text>
            <TextInput

              placeholder="Enter Your Name"
              style={{ paddingLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

            />
          </View>

          <View style={{ borderWidth: 1, borderColor: 'gray', height: 65, width: 340, alignSelf: 'center', borderRadius: 8, marginTop: 40 }}>
            <Text style={{ paddingLeft: 15, color: 'black', marginTop: 7 }}>Email Address</Text>
            <TextInput
              textContentType='emailAddress'
              placeholder="Enter Your Email"
              style={{ paddingLeft: 15, fontSize: 15, fontWeight: 'bold', color: 'black' }}

            />
          </View>





        </View>
      
        <TouchableOpacity 
          onPress={() => { signIn() }}

        >

          <Text style={{ color: 'white', marginTop: '5%' }}>CONTINUE</Text>

            <View style={styles.buttonStyle}>
          <Text style={{ color: 'white' }}>CONTINUE</Text>
          </View>

        </TouchableOpacity>
      </LinearGradient>

    </View>







  )
}



export default NameScreen;

const styles = StyleSheet.create({




  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginLeft: 35,
    marginRight: 20

  },

  searchBar: {

    flex: 1,
    // borderWidth: 1,
    // borderColor: '#EFEFEF',
    // borderRadius: 8,

    marginTop: 10,
    fontSize: 50,

    alignSelf: 'center'
  },
  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },

  buttonStyle: {
    backgroundColor: '#8ebc0e',

    marginTop: '10%',
    borderRadius: 10,



    alignItems: 'center',
    width: '84%',
    height: '14%',

    // position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: '83%',
    height: '29%',

    alignSelf: 'center',


  },
})
