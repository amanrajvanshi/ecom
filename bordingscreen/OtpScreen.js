import React, { Component } from 'react';

import { View, Text, TextInput, SafeAreaView, StyleSheet, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { AuthContext } from '../Autantication/Context'
import Icon1 from 'react-native-vector-icons/AntDesign';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

export default class OtpScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pin1: "",
      pin2: "",
      pin3: "",
      pin4: "",
      otp: ""

    };
  }

  componentDidMount() {



    this.refs.pin1ref.focus()
  }



  render() {
    const { pin1, pin2, pin3, pin4 } = this.state;




    return (
      <View style={{ flex: 1 }}>
        <LinearGradient
          // Background Linear Gradient
          colors={['rgba(245,159,0,0.5)', 'transparent']}
          style={styles.linearGradient}
        >
          <View >
            <View style={styles.container}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Verify Phone Number')}>


                <Ionicons name="chevron-back-outline" size={25} color="#222" backgroundColor="white" />

              </TouchableOpacity>
              <View style={styles.searchBar}>
                <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 10 }}>Phone Verification</Text>

              </View>

            </View>

          </View>
          <Text style={styles.text5}>Enter Your OTP code here</Text>
          <View style={{ justifyContent: "space-evenly", flexDirection: "row", marginTop: 50, }}>
            <TextInput

              keyboardType="numeric"
              ref={'pin1ref'}
              value={pin1}

              maxLength={1}
              onChangeText={(pin1) => {
                this.setState({ pin1: pin1 })
                if (pin1 != "") {
                  this.refs.pin2ref.focus()
                }
              }}

              style={{ backgroundColor: 'white', color: "black", fontWeight: '600', padding: 15, fontSize: 20, height: 55, width: "13%", borderRadius: 10, borderWidth: 0.5, borderColor: 'gray' }}
            />

            <TextInput

              keyboardType="numeric"
              ref={'pin2ref'}
              value={pin2}
              onChangeText={(pin2) => {
                this.setState({ pin2: pin2 })
                if (pin2 != "") {
                  this.refs.pin3ref.focus()
                }
                else {
                  this.refs.pin1ref.focus()
                }
              }}
              maxLength={1}


              style={{ backgroundColor: 'white', color: "black", fontWeight: '600', alignSelf: "center", padding: 15, alignItems: 'center', fontSize: 20, height: 55, width: "13%", borderRadius: 10, borderWidth: 0.5, borderColor: 'gray' }}
            />

            <TextInput

              keyboardType="numeric"
              ref={'pin3ref'}
              value={pin3}
              onChangeText={(pin3) => {
                this.setState({ pin3: pin3 })
                if (pin3 != "") {
                  this.refs.pin4ref.focus()

                }
                else {
                  this.refs.pin2ref.focus()
                }
              }}
              maxLength={1}

              style={{ backgroundColor: 'white', fontWeight: '600', color: "black", alignSelf: "center", padding: 15, alignItems: 'center', fontSize: 20, height: 55, width: "13%", borderRadius: 10, borderWidth: 0.5, borderColor: 'gray' }}
            />

            <TextInput


              keyboardType="numeric"
              ref={'pin4ref'}
              value={pin4}
              onChangeText={(pin4) => {
                this.setState({ pin4: pin4 })
                if (pin4 == "") {
                  this.refs.pin3ref.focus()
                }
              }
              }
              maxLength={1}

              style={{ backgroundColor: 'white', fontWeight: '600', color: "black", alignSelf: "center", padding: 15, alignItems: 'center', fontSize: 20, height: 55, width: "13%", borderRadius: 10, borderWidth: 0.5, borderColor: 'gray' }}
            />

          </View>
          <TouchableOpacity style={styles.buttonStyle}
            //    onPress={() =>navigation.navigate('Phone Varification') }
            onPress={() => this.props.navigation.navigate('Name')}
          >
            <Text style={styles.text2}>CONTINUE</Text>
          </TouchableOpacity>
          <Text style={styles.text1}>Didn't you received any code?</Text>
          <TouchableOpacity>
            <Text style={styles.text3}>Resend a new code</Text>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    )

  }


}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginLeft: 13,
    marginRight: 13

  },

  searchBar: {
    flexDirection: 'row',
    flex: 1,
    // borderWidth: 1,
    // borderColor: '#EFEFEF',
    // borderRadius: 8,
    paddingHorizontal: 60,
    paddingVertical: 10,
    fontSize: 50,
    alignItems: 'center',
    marginHorizontal: 12,
    textAlign: 'center',
    alignSelf: 'center'
  },

  scrollView: {

    marginBottom: 145,
  },

  input: {
    fontSize: 18,
    marginLeft: 10,
    paddingVertical: 5,
  },
  badge: {
    position: 'absolute',
    backgroundColor: '#E6848C',
    width: 18,
    height: 18,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    right: -4,
    top: -4,
  },
  badgeText: {
    color: '#fff',
  },
  cart: {
    marginRight: 10
  },

  text5: {
    fontSize: 15,
    marginLeft: 23,
    marginBottom: 10,
    marginTop: 20,
    color: 'gray',



  }

  ,

  text: {
    fontSize: 18,
    marginLeft: 19,
    marginBottom: 10,
    marginTop: 20,
    color: 'black',
  },

  text1: {
    fontSize: 15,
    marginLeft: 23,

    marginTop: 120,
    color: 'gray',

  },

  text2: {

    color: 'white',
  },

  text3: {
    fontSize: 15,
    marginLeft: 23,

    marginTop: 10,
    color: 'orange',
  },

  buttonStyle: {
    backgroundColor: '#67BC45',

    marginTop: 240,
    borderRadius: 10,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 330,
    height: 55,
    alignSelf: 'center',
    backgroundColor: "#E8AA00",

  },
})