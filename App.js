import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



import AsyncStorage from '@react-native-async-storage/async-storage';

import NumberScreen from './bordingscreen/NumberScreen';

import OnboardingScreen from './bordingscreen/OnboardingScreen'

import { createDrawerNavigator } from '@react-navigation/drawer';
import MainTabScreen from './StackScreen/MainTabScreen';
import DrawerContent from './screens/drawerScreen/DrawerContent'
import SupportScreen from './screens/drawerScreen/SupportScreen'
import SettingScreen from './screens/drawerScreen/SettingScreen'
import AddressScreen from './screens/drawerScreen/AddressScreen'

// import { StatusBar } from 'react-native';
import OtpScreen from './bordingscreen/OtpScreen';
import NameScreen from './bordingscreen/NameScreen';



// import { ActivityIndicator, View } from 'react-native';
import { AuthContext } from './Autantication/Context'
import { StatusBar } from 'react-native';


const AppStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const App = () => {




  const [isFirstLaunch, setIsFirstLaunch] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(() => ({
    signIn: () => {
      setUserToken('fcgh');
      setIsLoading(false);
    },
    signOut: () => {
      setUserToken('null');
      setIsLoading(false);
    },
    signUp: () => {
      setUserToken('skdfjsk');
      setIsLoading(false);
    },
  }));


  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  // if(isLoading){
  //   return(
  //     <View style ={{flex:1,justifyContent:'center',alignItems:'center'}}>
  //       <ActivityIndicator size="large"/>
  //     </View>
  //   );
  // }








  useEffect(() => {
    AsyncStorage.getItem('alreadyLaunched').then(value => {
      if (value == null) {
        AsyncStorage.setItem('alreadyLaunched', 'true');
        setIsFirstLaunch(true);

      } else {
        setIsFirstLaunch(false);
      }
    })
  }, []);

  if (isFirstLaunch === null) {
    return null;
  } else if (isFirstLaunch === true) {
    return (
      <AuthContext.Provider value={authContext}>
        <NavigationContainer>





          {userToken != null ? (



            <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
              <Drawer.Screen name="Home" component={MainTabScreen} />
              <Drawer.Screen name="Support" component={SupportScreen} />
              <Drawer.Screen name="Setting" component={SettingScreen} />
              <Drawer.Screen name="Address" component={AddressScreen} />
            </Drawer.Navigator>




          )
            :

            <AppStack.Navigator

            >
              <AppStack.Screen options={{ headerShown: false }} name="Onboarding" component={OnboardingScreen}
              />
              <AppStack.Screen options={{ headerShown: false }} name="Verify Phone Number" component={NumberScreen} />

              <AppStack.Screen options={{ headerShown: false }} name="Phone Verification" component={OtpScreen} />

              <AppStack.Screen options={{ headerShown: false }} name="Name" component={NameScreen} />

              {/* <AppStack.Screen options={{ headerShown: false }} name="Home" component={MainTabScreen} /> */}




            </AppStack.Navigator>
          }

        </NavigationContainer>
      </AuthContext.Provider>

    )
  } else {
    return (

      <NavigationContainer>

        <StatusBar barStyle='dark-content' backgroundColor='#F3F4F8' />


        <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
          <Drawer.Screen name="Home" component={MainTabScreen} />
          <Drawer.Screen name="Support" component={SupportScreen} />
          <Drawer.Screen name="Setting" component={SettingScreen} />
          <Drawer.Screen name="Address" component={AddressScreen} />
        </Drawer.Navigator>

      </NavigationContainer>


    )

  }





}

export default App;